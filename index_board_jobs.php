<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Soft-Board Jobs Today</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
    <meta http-equiv="refresh" content="30" >
</head>
<body style="background: url(images/circle-back.jpg) no-repeat; background-position: top center;">

<?php

	include("msconfig.php");

	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
	$current_week = (INT)date('W');
	$current_year = date("Y");
	$time_now = date ('H:i:s');

?>

<div id="container">
	<p style="margin: 5px 0 0 0; padding: 0; font-size: 30px; text-align: center; font-weight: bold">Jobs Pending This Week</p>

<?php /*?>
	<div id="message">
		<?php echo $message.'Date Today: '.$date_today;?>,
		<?php echo $message.'Current Week: '.$current_week;?>,
		<?php echo $message.'Current Year: '.$current_year;?>,
	</div>
<?php */?>

	<table align="center">
	<tr>
	<td valign="top">
	  <p align="center" style="margin: 0 0 5px 0; padding: 0; font-size: 30px; font-weight: bold">Today</p>
	  <table cellpadding=10 class="tbljobstoday">
	    <colgroup style="width: 150px"></colgroup>
	    <colgroup style="width: 100px"></colgroup>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <th>Agent</th>
	      <th>Jobs</th>
	    </tr>
	    
  <?php

	$today = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( GETDATE() AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName ORDER BY Jobs DESC");
	$i = 0;
	while ($row = mssql_fetch_array($today)){
		$i++;
		$name = $row['nName'];
		$jobs = $row['Jobs'];
?>
	    
	    <tr class="tr<?php echo ($i & 1) ?>">
	      <td><?php echo $name ?></td>
	      <td style="text-align: center; <?php /*?>color: <?php if ($jobs <= 2) echo 'red'; else if ($jobs >= 3 && $jobs <= 4) echo 'yellow'; else if ($jobs >= 5) echo 'green' ?><?php */?>"><?php echo $jobs ?></td>
	    </tr>
	    
  <?php

	}

	mssql_free_result($today);

	$today2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( GETDATE() AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($today2);
?>
	    
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <td>Total</td>
	      <td><?php echo $row['Total']; ?></td>
	      </tr>
	    </table>
	  </td>
	<td valign="top"><p align="center" style="margin: 0 0 5px 0; padding: 0; font-size: 30px; font-weight: bold">This Week</p>
	  <table cellpadding=10 class="tbljobstoday">
	    <colgroup style="width: 150px"></colgroup>
	    <colgroup style="width: 100px"></colgroup>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <th>Agent</th>
	      <th>Jobs</th>
	    </tr>
<?php

	mssql_free_result($today2);

	$thisweek = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE DATEPART(week, WorkDate) = DATEPART(week, GETDATE()) AND DATEPART(yyyy, WorkDate) = DATEPART(yyyy, GETDATE()) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName ORDER BY Jobs DESC");
	$i = 0;
	while ($row = mssql_fetch_array($thisweek)){
		$i++;
		$name = $row['nName'];
		$jobs = $row['Jobs'];
?>
	    <tr class="tr<?php echo ($i & 1) ?>">
	      <td><?php echo $name ?></td>
	      <td style="text-align: center; <?php /*?>color: <?php if ($jobs <= 2) echo 'red'; else if ($jobs >= 3 && $jobs <= 4) echo 'yellow'; else if ($jobs >= 5) echo 'green' ?><?php */?>"><?php echo $jobs ?></td>
	      </tr>
<?php

	}

	mssql_free_result($thisweek);

	$thisweek2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE DATEPART(week, WorkDate) = DATEPART(week, GETDATE()) AND DATEPART(yyyy, WorkDate) = DATEPART(yyyy, GETDATE()) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($thisweek2);
?>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <td>Total</td>
	      <td><?php echo $row['Total']; ?></td>
	      </tr>
	    </table></td>
	<td valign="top">
	  <p align="center" style="margin: 0 0 5px 0; padding: 0; font-size: 30px; font-weight: bold">Tomorrow</p>
	  <table cellpadding=10 class="tbljobstoday">
	    <colgroup style="width: 150px"></colgroup>
	    <colgroup style="width: 100px"></colgroup>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <th>Agent</th>
	      <th>Jobs</th>
	    </tr>
	    
  <?php
  
	mssql_free_result($thisweek2);

	$tomorrow = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( DATEADD(dd,1,GETDATE()) AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName ORDER BY Jobs DESC");
	$i = 0;
	while ($row = mssql_fetch_array($tomorrow)){
		$i++;
		$name = $row['nName'];
		$jobs = $row['Jobs'];
?>
	    
	    <tr class="tr<?php echo ($i & 1) ?>">
	      <td><?php echo $name ?></td>
	      <td style="text-align: center; <?php /*?>color: <?php if ($jobs <= 2) echo 'red'; else if ($jobs >= 3 && $jobs <= 4) echo 'yellow'; else if ($jobs >= 5) echo 'green' ?><?php */?>"><?php echo $jobs ?></td>
	    </tr>
	    
  <?php

	}

	mssql_free_result($tomorrow);

	$tomorrow2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( DATEADD(dd,1,GETDATE()) AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($tomorrow2);
?>
	    
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <td>Total</td>
	      <td><?php echo $row['Total']; ?></td>
	      </tr>
	    </table>
	  </td>
	</tr>
	</table>
</div>
<br/>

<?php

	mssql_free_result($tomorrow2);
	mssql_close($con);
?>
</body>
</html>