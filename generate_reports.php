<?php
session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
	
include("config.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Report Generation Form</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/south-street/jquery-ui.css" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
        <script>
            $(function() {
              $( "#from" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#to" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                }
              });
              $( "#datebooked" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#dateofjob" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#accountsboardfromdate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#accountsboardtodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#telesalesfromdate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#telesalestodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#roadsweeperfromdate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#roadsweepertodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#cctwofromdate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#cctwotodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              
              $( "#telesalestodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#telesalestodate" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
            });
            
        </script>
        <script>
            $(function() {
                $('#selectBy').change(function(){
                    $('.reports').hide();
                    $('#' + $(this).val()).show();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <h2><span class="label label-primary">Report Generation</span></h2>
            <br><br> 
            <div class="form-group col-lg-6">
                <label for="selectBy">Generate report based on:</label>
                <select class="form-control" name="selectBy" id="selectBy" required>
                    <option></option>
                    <optgroup>                       
                        <option value="fromtodate">From-To Date</option>
                        <option value="bookeddate">Booked date</option>
                        <option value="jobdate">Job date</option>
                    </optgroup>
                    <optgroup>
                        <option value="agentname">Agent Name</option>
                        <option value="companyname">Company Name</option>
                    </optgroup>
                    <optgroup>
                        <option value="cancelledjobs">Cancelled jobs</option>
                        <option value="removedjobs">Removed jobs</option>
                    </optgroup>
                </select>
            </div>
            <div id="fromtodate" class="reports" style="display:none">
                <form role="form" action="download_report.php?type=fromtodate" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="from">From:</label>
                        <input type="text" class="form-control" id="from" name="fromDate" required>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="to">To:</label>
                        <input type="text" class="form-control" id="to" name="toDate" required>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                        
                        <div class="form-group col-lg-10"></div>
                        <label for="selectDate">Booked Date/Job Date:</label>
                        <select class="form-control" name="selectDate" required>
                            <option></option>
                            <option>Booked Date</option>
                            <option>Job Date</option>
                        </select>
                        
                        <br><br>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Accounts">Accounts Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Telesales">Tele-sales Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Roadsweeper">Road Sweeper Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Cctwo">Call Centre - 2 Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Allboard">All 
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="CancelledJobs">Cancelled Jobs
                        </label>
                     </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            <div id="bookeddate" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=bookeddate" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="datebooked">Booked Date:</label>
                        <input type="text" class="form-control" id="datebooked" name="datebooked" required>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                        <br><br>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Accounts">Accounts Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Telesales">Tele-sales Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Roadsweeper">Road Sweeper Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Cctwo">Call Centre - 2 Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Allboard">All 
                        </label>
                     </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>            
            
            <div id="jobdate" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=jobdate" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="dateofjob">Job Date:</label>
                        <input type="text" class="form-control" id="dateofjob" name="dateofjob" required>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                        <br><br>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Accounts">Accounts Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Telesales">Tele-sales Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Roadsweeper">Road Sweeper Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Cctwo">Call Centre - 2 Board
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radioboard" value="Allboard">All 
                        </label>
                     </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            
            <div id="agentname" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=agentname" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectagentname">Agent Name:</label>
                        <select class="form-control" id="sel1" name="selectagentname" required>
                                <option></option>
                                <?php 
                                $result = mysql_query("select agent_name from agents");
                                    while($row = mysql_fetch_array($result)) {
                                        echo '<option value="'.$row['agent_name'].'">'.$row['agent_name'].'</option>';
                                }
                                ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                     </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>  
            <div id="companyname" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=companyname" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="usr">Company Name:</label>
                        <select class="form-control" id="sel1" name="company" required>
                                <option></option>
                                <?php 
                                $result = mysql_query("select DISTINCT company_name from (select company_name as company_name from Jobs union select company_name as company_name from apps) Jobs where company_name != ' '");
                                    while($row = mysql_fetch_array($result)) {
                                        echo '<option value="'.$row['company_name'].'">'.$row['company_name'].'</option>';
                                }
                                ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                     </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            
            <div id="cancelledjobs" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=cancelledjobs" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectcancelledjobs">Cancelled Jobs/Appointments</label>
                        <select class="form-control" name="selectcancelledjobs" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                        <br><br>
                        <label class="radio-inline">
                            <input type="radio" name="formradio" value="Today">Today
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formradio" value="Week">Week
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formradio" value="Month">Month
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formradio" value="Year">Year
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formradio" value="Beginning">From beginning
                        </label>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            <div id="removedjobs" class="reports" style="display: none">
                <form role="form" action="download_report.php?type=removedjobs" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="selectremovedjobs">Removed Jobs/Appointments</label>
                        <select class="form-control" name="selectremovedjobs" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                            <option>Jobs and Appointments</option>
                        </select>
                        <br><br>
                        <label class="radio-inline">
                            <input type="radio" name="formtworadio" value="Today">Today
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formtworadio" value="Week">Week
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formtworadio" value="Month">Month
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formtworadio" value="Year">Year
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="formtworadio" value="beginning">From beginning
                        </label>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            
        </div>
    </body>
</html>


             
            