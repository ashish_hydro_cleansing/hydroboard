<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Board Reports - This Month/Last Month</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" src="calendar_db.js"></script>
</head>
<body>

<?php

	include("config.php");

	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
	$current_week = (INT)date('W');
	$current_month = date('n');
	$current_year = date('Y');
	$last_week = $current_week - 1;
	$last_month = $current_month - 1;
	$last_year = date("Y");
	$time_now = date ('H:i:s');

	if ($last_month == 0) {
			$last_month = 12;
			$last_year --;
	}

	function getAppsWeek($week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAppsMonth($month_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && MONTH(booked_date) = '$month_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAgentAppsWeek($agent_search,$week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAgentAppsMonth($agent_search,$month_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && MONTH(booked_date) = '$month_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
?>

<div id="container">

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Board Reports - This Month/Last Month</h3>
	</div>

	<div align="center">
		<form class="menu">
			<?php include("reports_menu.php"); ?>
		</form>
	</div>

	<div id="message">
		<?php echo $message.'Date Today: '.$date_today;?>,
		<?php echo $message.'Current Month: '.$current_month;?>,
		<?php echo $message.'Current Year: '.$current_year;?>,
		<?php echo $message.'Last Month: '.$last_month;?>,
		<?php echo $message.'Last Year: '.$last_year;?>
	</div>

	<div id="reports">
	<table align="center" >
	<tr>
	<td valign="top">
		<p align="center">This Month's Jobs and Apps</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 104px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#FF6600' style="text-align: center; font-weight: bold">
				<th>Week</th>
				<th>Starting</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>
<?php

	$thismonth = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date) ORDER BY booked_date ASC");
	$i = 0;
	while ($row = mysql_fetch_array($thismonth)){
		$appsweek = $row['booked_date'];
		$weeknum = date('W', strtotime($appsweek));
		$starting = date('d/m/Y', strtotime($appsweek));
		$number_of_apps = getAppsWeek($weeknum,$current_year);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $weeknum ?></td>
				<td><?php echo $starting ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>
<?php

	}

	$thismonth2 = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1");
	$row = mysql_fetch_array($thismonth2);
	$number_of_apps = getAppsMonth($current_month,$current_year);
?>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<td>Total</td>
				<td></td>
				<td><?php echo $row['SUM(job_value)']; ?></td>
				<td><?php echo $row['SUM(points_value)']; ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
		</table>
	</td>
	<td valign="top">
		<p align="center">Last Month's Jobs and Apps</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 104px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Week</th>
				<th>Starting</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>
<?php

	$lastmonth = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $last_year && MONTH(booked_date) = $last_month && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date) ORDER BY booked_date ASC");
	$i = 0;
	while ($row = mysql_fetch_array($lastmonth)){
		$appsweek = $row['booked_date'];
		$weeknum = date('W', strtotime($appsweek));
		$starting = date('d/m/Y', strtotime($appsweek));
		$number_of_apps = getAppsWeek($weeknum,$last_year);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $weeknum ?></td>
				<td><?php echo $starting ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>
<?php

	}

	$lastmonth2 = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $last_year && MONTH(booked_date) = $last_month && Cancelled != 1 && Removed != 1");
	$row = mysql_fetch_array($lastmonth2);
	$number_of_apps = getAppsMonth($last_month,$last_year);
?>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<td>Total</td>
				<td></td>
				<td><?php echo $row['SUM(job_value)']; ?></td>
				<td><?php echo $row['SUM(points_value)']; ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
		</table>
	</td>
	</tr>
	<tr>
	<td valign="top">
		<p align="center">This Month's Leaderboards</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 174px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Agent</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	include("config.php");

	$agentsquery = "CREATE TEMPORARY TABLE LeaderBoard (
		`agent_name` varchar(20) NOT NULL,
		`job_number` int(5) NOT NULL,
		`job_value` decimal(3,1) NOT NULL,
		`points_value` decimal(8,2) NOT NULL
	);";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`, `job_number`, `job_value`, `points_value`) SELECT agent_name, job_number, job_value, points_value FROM jobs WHERE YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`) SELECT DISTINCT agent_name FROM apps WHERE YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "SELECT agent_name, SUM(job_value), SUM(points_value) FROM leaderboard GROUP BY agent_name ORDER BY SUM(points_value) DESC;";
	$agentsquery .= "DROP TABLE LeaderBoard";

	mysqli_multi_query($link, $agentsquery) or die("MySQL Error: " . mysqli_error($link) . "<hr>\nQuery: $agentsquery");
	mysqli_next_result($link);
	mysqli_next_result($link);
	mysqli_next_result($link);

	if ($result = mysqli_store_result($link)) {
		$i = 0;
		while ($row = mysqli_fetch_array($result)){
			$number_of_apps = getAgentAppsMonth($row['agent_name'],$current_month,$current_year);
			$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
			
<?php

		}
	}
?>

		</table>
	</td>
	<td valign="top">
		<p align="center">Last Month's Leaderboards</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 174px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Agent</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	include("config.php");

	$agentsquery = "CREATE TEMPORARY TABLE LeaderBoard (
		`agent_name` varchar(20) NOT NULL,
		`job_number` int(5) NOT NULL,
		`job_value` decimal(3,1) NOT NULL,
		`points_value` decimal(8,2) NOT NULL
	);";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`, `job_number`, `job_value`, `points_value`) SELECT agent_name, job_number, job_value, points_value FROM jobs WHERE YEAR(booked_date) = $last_year && MONTH(booked_date) = $last_month && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`) SELECT DISTINCT agent_name FROM apps WHERE YEAR(booked_date) = $last_year && MONTH(booked_date) = $last_month && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "SELECT agent_name, SUM(job_value), SUM(points_value) FROM leaderboard GROUP BY agent_name ORDER BY SUM(points_value) DESC;";
	$agentsquery .= "DROP TABLE LeaderBoard";

	mysqli_multi_query($link, $agentsquery) or die("MySQL Error: " . mysqli_error($link) . "<hr>\nQuery: $agentsquery");
	mysqli_next_result($link);
	mysqli_next_result($link);
	mysqli_next_result($link);

	if ($result = mysqli_store_result($link)) {
		$i = 0;
		while ($row = mysqli_fetch_array($result)){
			$number_of_apps = getAgentAppsMonth($row['agent_name'],$last_month,$last_year);
			$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
			
<?php

		}
	}
?>

		</table>
	</td>
	</tr>
	</table>
	</div>
</div>
<br/>

<?php

	mysqli_close($link);
	mysql_close($con);
?>
</body>
</html>