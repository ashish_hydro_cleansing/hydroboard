<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
			function renderData(rs){
				var appsTodayDiv = document.getElementsByClassName("apps day")[0];
				appsTodayDiv.classList.add(rs.apps.today.colour);
				appsTodayDiv.innerHTML = rs.apps.today.figure;
				
				var pointsTodayDiv = document.getElementsByClassName("points day")[0];
				pointsTodayDiv.classList.add(rs.points.today.colour);
				pointsTodayDiv.innerHTML = rs.points.today.figure;
				
				var jobsTodayDiv = document.getElementsByClassName("jobs day")[0]; 
				jobsTodayDiv.classList.add(rs.jobs.today.colour);
				jobsTodayDiv.innerHTML = rs.jobs.today.figure;
				
				var appsWeekDiv = document.getElementsByClassName("apps week")[0];
				appsWeekDiv.classList.add(rs.apps.week.colour);
				appsWeekDiv.innerHTML = rs.apps.week.figure;
				
				var pointsWeekDiv = document.getElementsByClassName("points week")[0];
				pointsWeekDiv.classList.add(rs.points.week.colour);
				pointsWeekDiv.innerHTML = rs.points.week.figure;
				
				var jobsWeekDiv = document.getElementsByClassName("jobs week")[0];
				jobsWeekDiv.classList.add(rs.jobs.week.colour);
				jobsWeekDiv.innerHTML = rs.jobs.week.figure;
				
				setTimeout(function(){longPollData()},1000);
			}
		
			function loadData(){
				$.ajax({
					url:"model/OverallBoardBackEnd.php",
					type:"POST",
					dataType:"json",
					success: function(rs){
						console.log("poll");
						renderData(rs);
					}
				});
			}
		
            function setPaddingLeft(x) {
                if(x < 10){
                    x = 15;
                }
                document.getElementById("column-headings").style.paddingLeft = x + "px";
                var t = document.getElementById("table");
                document.getElementById("tableTop").style.left = x + "px";
                var rows = t.getElementsByClassName("row");
                for (var i = 0; i < rows.length; i++){
                    var ele = rows[i].children[rows[i].children.length - 1];
                    ele.style.paddingLeft = x + "px";
                }
            }
            function setTable() {
                var iw = window.innerWidth;
                var tw = document.getElementById("tableTop").offsetWidth;
                var os = (iw - tw) / 2;
                setPaddingLeft(os);
            }
			function longPollData(){
				setTimeout(function(){
					loadData();
				},1000);
			}			
			function loader(){
				setTable();
				loadData();
			}
            window.addEventListener("load",loader);
            window.addEventListener("resize",setTable);
        </script>
        <title>Overall Hydro-Board Wallboard</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <link href="css/board.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body id="overall">
    <svg style="display:none; position: absolute;" id="circle">
        <clipPath id="clip">
            <circle cx="0" cy="487.5" r="1290" fill="red"/>
        </clipPath>
    </svg>
        <div id="window"></div>
        <div id="screen">
                <div id="tableTop">
                    <div id="logo"><img src="logo.png"></div>
                    <div class="row" id="column-headings">
                        <div class="jobHeading column">JOBS</div>
                        <div class="pointHeading column">POINTS</div>
                        <div class="appsHeading column">APPOINTMENTS</div>
                        <div class="appsHeading column heading"></div>
                    </div>
                    <div id="table">
                        <div class="row" id="day">
                            <div class="apps day column"></div>
                            <div class="points day column"></div>
                            <div class="jobs day column"></div>
                        </div>
                        <div class="row" id="week">
                            <div class="apps week column"></div>
                            <div class="points week column"></div>
                            <div class="jobs week column"></div>
                        </div>
                        <div class="sideheading">
                            <div class="day column heading">
                                <div class="caption">
                                    <span>today</span><span class="yellText date"><?php echo $date_today?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date("l", mktime())); ?></span>
                                </div>
                            </div>
                            <div class="day column heading">
                                <div class="caption">
                                    <span>this</span>
                                    <br>
                                    <span class="bold">WEEK</span><span class="yellText"><?php echo $current_week?></span>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                    <div id="boardBanner">
                        <div id="boardHeader"><h1>Overall Target Board</h1></div>
                        <div id="key">
                            <span class="red">below</span>
                            <span class="orange">near</span>
                            <span class="green">achieved</span>
                            <span class="gold">exceeded</span>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>