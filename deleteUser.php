<?php
include("config.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  *  author : reka sambath
  */
  session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Delete Agent </title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2><span class="label label-primary">Disable Agent</span></h2>
            <br><br>            
            <div class="alert alert-success">
               <?php
            if($_GET['msg'] == "agentDeleted"){ ?>
                <strong>Success!</strong> <?php 
                echo "Access for"." ".$_GET['agent_name']." "."was disabled";
            }
            else{
             echo "Disable the access for an Agent here.";   
            }
            ?>
        </div>
            <form role="form" method="post" action="agentOperations.php?cmd=delete_agent">
                <div class="form-group col-lg-6">
                    <label for="usr">Agent Name:</label>
                    <select class="form-control" id="sel1" name="enabled_agent" required>
                            <option></option>
                            <?php 
                            $result = mysql_query("select * from agents where enabled = 1");
                                while($row = mysql_fetch_array($result)) {
                                    echo '<option value="'.$row['agent_name'].'">'.$row['agent_name'].'</option>';
                            }
                            ?>
                    </select>
                </div>
                <div class="col-lg-10"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-primary">Disable</button>
                    <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button>
                </div>
            </form>
        </div>
    </body>
</html>    
