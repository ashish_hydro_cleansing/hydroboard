
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        
        <link href='calendar/jquery-ui.min.css' rel='stylesheet' />
        <link href='calendar/jquery-ui.theme.min.css' rel='stylesheet' />
        <link href='calendar/jquery-ui.structure.min.css' rel='stylesheet' />
        <script src='calendar/jquery-ui.min.js'></script>
        
        <link href='calendar/fullcalendar.css' rel='stylesheet' />
        <link href='calendar/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src='calendar/moment.min.js'></script>
        <script src='calendar/jquery.min.js'></script>
        <script src='calendar/fullcalendar.min.js'></script>
<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
                            },
                        buttonText: {
                                prev: '<',
                                next: '>'
			},
                        theme: true,
                        firstDay: 1,
                        fixedWeekCount: false,
                        weekNumbers: true,
                        eventLimit: 50,
                        selectable: true,
                        eventSources: [
                            {
                                url: 'HclAppointments.php'
                            },
                            {
                                    url: 'HclBookings.php'
                            },
                            {
                                url: 'daytodayevents.php'
                            }
                        ],
                        dayClick: function(date, jsEvent, view) {
                            var viewname = view.name;
                            if(viewname === 'month' || viewname === 'basicWeek'){
                                $('#calendar').fullCalendar( 'changeView', 'basicDay');
                                $('#calendar').fullCalendar('gotoDate', date); 
                                
                            }
                        },
                        
                            
                        eventRender: function(event, element, view) {
                            var view = $('#calendar').fullCalendar('getView');
                            if(event.title > 300) {
                                element.find('.fc-title').append(' - Points'); 
                                element.css('background-color', '#00CD66');
                            }
                            if(event.title < 300){
                                element.find('.fc-title').append(' - Points'); 
                            }
                            if (view.name === 'basicDay') {
                                element.find('.fc-title').append("<br/>" + event.description); 
                            }
                            if(view.name === 'month' || view.name === 'basicWeek'){
                                if(event.id === "three"){
                                    element.hide();
                                }
                            }
                        },  
        
                        eventClick: function(date, jsEvent, view){
                            if(view.name === 'month' || view.name === 'basicWeek'){
                                $('#calendar').fullCalendar('removeEvents', function (jsEvent) {
                                    if(jsEvent.id === "three"){
                                      return true;
                                    }
                                });
                            }
                        }
                });
            });

</script>
</head>
<body style="background-color:#C6E2FF; ">
    <div class="container">
        <div class='row'>
            <div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div><div class='col-lg-10'></div>
        </div>
        <div id='calendar'></div>     
    </div>
	

</body>
</html>
