<?php
include("../config.php"); 
 
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{

// Green Main Target
$target_jt_day = 125/5;
$target_jw_week = 125;
$target_pt_day = 813/5;
$target_pw_week = 813;
$target_at_day = 15/5;
$target_aw_week = 15;

// Gold Threshold
$target_jt2_day = 150/5;
$target_jw2_week = 150;
$target_pt2_day = 975/5;
$target_pw2_week = 975;
$target_at2_day = 20/5;
$target_aw2_week = 20;

// BOARD SETUP
// 1 = Account Managers
// 2 = Telesales
// 3 = Road Sweeper Executives
// 4 = Managers
// 6 = Call Centre Two



// START - DATE SETUP (Change these to query specific dates/weeks)

$date_today = date("Y-m-d");
$day_number = date('w');
$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);

// END - DATE SETUP

$current_hour = date('G');
$current_min = date('i');

$target_jt_now = ($current_hour - 9) * $target_jt_day/8 + ($current_min * $target_jt_day/(8*60)); 
$target_jw_now = ($day_number - 1) * $target_jt_day + $target_jt_now; 

$target_pt_now = ($current_hour - 9) * $target_pt_day/8 + ($current_min * $target_pt_day/(8*60));
$target_pw_now = ($day_number - 1) * $target_pt_day + $target_pt_now; 

$target_at_now = ($current_hour - 9) * $target_at_day/8 + ($current_min * $target_at_day/(8*60)); 
$target_aw_now = ($day_number - 1) * $target_at_day + $target_at_now; 

$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.booked_date='$date_today' && j.cancelled = '0' && a.role_id = 5";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jt = $row['SUM(j.job_value)'];
$pt = $row['SUM(j.points_value)'];
/*$jt = 150/5;
$pt = 975/5;*/


$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.booked_date >= '$week_begin' && j.cancelled = '0' && a.role_id = 5";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jw= $row['SUM(j.job_value)'];
$pw= $row['SUM(j.points_value)'];
/*$jw= 150;
$pw= 975;*/

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.booked_date ='$date_today' && a.cancelled = '0' && ag.role_id = 5";
$result = mysql_query($query) or die(mysql_error());
$at = mysql_num_rows($result);
/*$at = 20/5;*/

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.booked_date >= '$week_begin' && a.cancelled = '0' && ag.role_id = 5";
$result = mysql_query($query) or die(mysql_error());
$aw = mysql_num_rows($result);
/*$aw = 20;*/

if ($jt > $target_jt_day) $target_jt_now = $target_jt_day;
if ($jw > $target_jw_week) $target_jw_now = $target_jw_week;
if ($pt > $target_pt_day) $target_pt_now = $target_pt_day;
if ($pw > $target_pw_week) $target_pw_now = $target_pw_week;
if ($at > $target_at_day) $target_at_now = $target_at_day;
if ($aw > $target_aw_week) $target_aw_now = $target_aw_week;

// Board Display Calcualtion (floor removes all digits beyond the decimal)
$jt1 = floor($jt/100);
$jt2 = floor(($jt - $jt1*100 )/10);
$jt3 = floor(($jt - $jt1*100 - $jt2*10));
$jw1 = floor($jw/100);
$jw2 = floor(($jw - $jw1*100 )/10);
$jw3 = floor(($jw - $jw1*100 - $jw2*10));
$pt0 = floor($pt/1000);
$pt1 = floor(($pt - $pt0*1000 )/100);
$pt2 = floor(($pt - $pt0*1000 - $pt1*100 )/10);
$pt3 = floor(($pt - $pt0*1000 - $pt1*100 - $pt2*10));
$pw0 = floor($pw/1000);
$pw1 = floor(($pw - $pw0*1000 )/100);
$pw2 = floor(($pw - $pw0*1000 - $pw1*100 )/10);
$pw3 = floor(($pw - $pw0*1000 - $pw1*100 - $pw2*10));
$at1 = floor($at/100);
$at2 = floor(($at - $at1*100 )/10);
$at3 = floor(($at - $at1*100 - $at2*10));
$aw1 = floor($aw/100);
$aw2 = floor(($aw - $aw1*100 )/10);
$aw3 = floor(($aw - $aw1*100 - $aw2*10));

function getColourCodeForTarget($value, $gold_target, $green_target, $orange_target){
	$colour;
		//if ($value >= $gold_target) $colour = 'gold'; 
		//else if ($value >= $green_target) $colour = 'green'; 
		//else if ($value > $orange_target) $colour = 'orange'; 
		//else $colour = 'red';
		$colour = 'blue';
	return $colour;
}
// Cells Colour Code Dependent On Given Cells Target
//today
//job
$jtcolour = getColourCodeForTarget($jt,$target_jt2_day,$target_jt_day,$target_jt_now);
//points
$ptcolour = getColourCodeForTarget($pt,$target_pt2_day,$target_pt_day,$target_pt_now);
//appointments
$atcolour = getColourCodeForTarget($at,$target_at2_day,$target_at_day,$target_at_now);

//week
//job
$jwcolour = getColourCodeForTarget($jw,$target_jw2_week,$target_jw_week,$target_jw_now);
//points
$pwcolour = getColourCodeForTarget($pw,$target_pw2_week,$target_pw_week,$target_pw_now);
//appointments
$awcolour = getColourCodeForTarget($aw,$target_aw2_week,$target_aw_week,$target_aw_now);

$current_week = (INT)date('W');
$date_today = date("d.m.y");

$arr = array(
	"apps" => array(
		"today" => array("figure"=>floor($at),"colour"=>$atcolour) ,
		"week" => array("figure"=>floor($aw),"colour"=>$awcolour)
	),
	"jobs" => array(
		"today" => array("figure"=>floor($jt),"colour"=>$jtcolour) ,
		"week" => array("figure"=>floor($jw),"colour"=>$jwcolour)
	),
	"points" => array(
		"today" => array("figure"=>floor($pt),"colour"=>$ptcolour) ,
		"week" => array("figure"=>floor($pw),"colour"=>$pwcolour)
	)
);

echo json_encode($arr);
} else {
	header("Location: http://hclremote:1280/hydroboard/roadsweepers_board.php");
	die();
}
?>
