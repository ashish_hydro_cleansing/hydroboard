<?php
include("../config.php"); 
 
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{

// Green Main Target
/*$target_jt_day = 125/5;
$target_jw_week = 125;
$target_pt_day = 150000/5;
$target_pw_week = 150000;
$target_at_day = 15/5;
$target_aw_week = 15;*/

// Gold Threshold
/*$target_jt2_day = 150/5;
$target_jw2_week = 150;
$target_pt2_day = 975/5;
$target_pw2_week = 975;
$target_at2_day = 20/5;
$target_aw2_week = 20;*/

// BOARD SETUP
// 1 = Account Managers
// 2 = Telesales
// 3 = Road Sweeper Executives
// 4 = Managers
// 6 = Call Centre Two



// START - DATE SETUP (Change these to query specific dates/weeks)

$date_today = date("Y-m-d");
$day_number = date('w');
$date_nextday = date('Y-m-d', strtotime($date_today. ' + 1 days'));
$nextday_number = strtoupper(date('w', strtotime($Date. ' + 1 days')));
$date_secondday = date('Y-m-d', strtotime($date_today. ' + 2 days'));
$secondday_number = strtoupper(date('w', strtotime($Date. ' + 2 days')));
$date_thirdday = date('Y-m-d', strtotime($date_today. ' + 3 days'));
$thirdday_number = strtoupper(date('w', strtotime($Date. ' + 3 days')));
$date_fourthday = date('Y-m-d', strtotime($date_today. ' + 4 days'));
$fourthday_number = strtoupper(date('w', strtotime($Date. ' + 4 days')));

$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
$month_begin = date('Y-m-d', strtotime("+30 days"));

// END - DATE SETUP

/*$current_hour = date('G');
$current_min = date('i');

$target_jt_now = ($current_hour - 9) * $target_jt_day/8 + ($current_min * $target_jt_day/(8*60)); 
$target_jw_now = ($day_number - 1) * $target_jt_day + $target_jt_now; 

$target_pt_now = ($current_hour - 9) * $target_pt_day/8 + ($current_min * $target_pt_day/(8*60));
$target_pw_now = ($day_number - 1) * $target_pt_day + $target_pt_now; 

$target_at_now = ($current_hour - 9) * $target_at_day/8 + ($current_min * $target_at_day/(8*60)); 
$target_aw_now = ($day_number - 1) * $target_at_day + $target_at_now;  */

$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_today' && j.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jt = $row['SUM(j.job_value)'];
$pt = $row['SUM(j.points_value)'];
/*$jt = 150/5;
$pt = 975/5;*/

/* Points by teams */
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_today' && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$ptWWM = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_today' && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$ptIB = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_today' && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$ptSS = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_today' && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$ptMD = $row['SUM(j.points_value)'];

/*Next day jobs and revenue or points*/
$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_nextday' && j.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jn = $row['SUM(j.job_value)'];
$pn = $row['SUM(j.points_value)'];

/* points by teams next day */
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_nextday' && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pnWWM = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_nextday' && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pnIB = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_nextday' && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pnSS = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_nextday' && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pnMD = $row['SUM(j.points_value)'];

/*Second day jobs and revenue or points*/
$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_secondday' && j.cancelled = '0' ";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$js = $row['SUM(j.job_value)'];
$ps = $row['SUM(j.points_value)'];

/*Points by teams second day */
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_secondday' && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$psWWM = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_secondday' && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$psIB = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_secondday' && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$psSS = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_secondday' && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$psMD = $row['SUM(j.points_value)'];

/*Third day jobs and revenue or points*/
$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_thirdday' && j.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jth = $row['SUM(j.job_value)'];
$pth = $row['SUM(j.points_value)'];

/*Points by teams third day */
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_thirdday' && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pthWWM = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_thirdday' && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pthIB = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_thirdday' && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pthSS = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_thirdday' && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pthMD = $row['SUM(j.points_value)'];


/*Fourth day jobs and revenue or points*/
$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_fourthday' && j.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jfo = $row['SUM(j.job_value)'];
$pfo = $row['SUM(j.points_value)'];

/*Points by teams Fourth day */
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_fourthday' && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pfoWWM = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_fourthday' && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pfoIB = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_fourthday' && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pfoSS = $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE j.job_date='$date_fourthday' && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pfoMD = $row['SUM(j.points_value)'];



/*$query = "SELECT SUM(j.job_value),SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE MONTH(j.job_date) = MONTH(CURRENT_DATE()) && YEAR(j.job_date) = YEAR(CURRENT_DATE()) && j.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jw= $row['SUM(j.job_value)'];
$pw= $row['SUM(j.points_value)'];
$jw= 150;
$pw= 975;*/

/*Points for week for all teams
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE MONTH(j.job_date) = MONTH(CURRENT_DATE()) && YEAR(j.job_date) = YEAR(CURRENT_DATE()) && j.cancelled = '0' && j.group_id = '1'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pwWWM= $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE MONTH(j.job_date) = MONTH(CURRENT_DATE()) && YEAR(j.job_date) = YEAR(CURRENT_DATE()) && j.cancelled = '0' && j.group_id = '2'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pwIB= $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE MONTH(j.job_date) = MONTH(CURRENT_DATE()) && YEAR(j.job_date) = YEAR(CURRENT_DATE()) && j.cancelled = '0' && j.group_id = '3'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pwSS= $row['SUM(j.points_value)'];
$query = "SELECT SUM(j.points_value) FROM jobs j JOIN agents a ON a.agent_name = j.agent_name WHERE MONTH(j.job_date) = MONTH(CURRENT_DATE()) && YEAR(j.job_date) = YEAR(CURRENT_DATE()) && j.cancelled = '0' && j.group_id = '4'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$pwMD = $row['SUM(j.points_value)']; */


/*$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.app_date ='$date_today' && a.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$at = mysql_num_rows($result);

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.app_date ='$date_nextday' && a.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$an = mysql_num_rows($result);

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.app_date ='$date_secondday' && a.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$as = mysql_num_rows($result);

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.app_date ='$date_thirdday' && a.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$ath = mysql_num_rows($result);

$query = "SELECT a.* FROM apps a JOIN agents ag ON a.agent_name = ag.agent_name WHERE a.app_date <= '$month_begin' && a.app_date >= '$date_today' && a.cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$aw = mysql_num_rows($result);
*/

/*if ($jt > $target_jt_day) $target_jt_now = $target_jt_day;
if ($jw > $target_jw_week) $target_jw_now = $target_jw_week;
if ($pt > $target_pt_day) $target_pt_now = $target_pt_day;
if ($pw > $target_pw_week) $target_pw_now = $target_pw_week;
if ($at > $target_at_day) $target_at_now = $target_at_day;
if ($aw > $target_aw_week) $target_aw_now = $target_aw_week;

// Board Display Calcualtion (floor removes all digits beyond the decimal)
$jt1 = floor($jt/100);
$jt2 = floor(($jt - $jt1*100 )/10);
$jt3 = floor(($jt - $jt1*100 - $jt2*10));
$jw1 = floor($jw/100);
$jw2 = floor(($jw - $jw1*100 )/10);
$jw3 = floor(($jw - $jw1*100 - $jw2*10));
$pt0 = floor($pt/1000);
$pt1 = floor(($pt - $pt0*1000 )/100);
$pt2 = floor(($pt - $pt0*1000 - $pt1*100 )/10);
$pt3 = floor(($pt - $pt0*1000 - $pt1*100 - $pt2*10));
$pw0 = floor($pw/1000);
$pw1 = floor(($pw - $pw0*1000 )/100);
$pw2 = floor(($pw - $pw0*1000 - $pw1*100 )/10);
$pw3 = floor(($pw - $pw0*1000 - $pw1*100 - $pw2*10));
$at1 = floor($at/100);
$at2 = floor(($at - $at1*100 )/10);
$at3 = floor(($at - $at1*100 - $at2*10));
$aw1 = floor($aw/100);
$aw2 = floor(($aw - $aw1*100 )/10);
$aw3 = floor(($aw - $aw1*100 - $aw2*10));

function getColourCodeForTarget($value, $gold_target, $green_target, $orange_target){
	$colour;		
	$colour = 'blue';
	return $colour;
}

// Cells Colour Code Dependent On Given Cells Target
//today
//job
$jtcolour = getColourCodeForTarget($jt,$target_jt2_day,$target_jt_day,$target_jt_now);
//points
$ptcolour = getColourCodeForTarget($pt,$target_pt2_day,$target_pt_day,$target_pt_now);
//appointments
$atcolour = getColourCodeForTarget($at,$target_at2_day,$target_at_day,$target_at_now);

//week
//job
$jwcolour = getColourCodeForTarget($jw,$target_jw2_week,$target_jw_week,$target_jw_now);
//points
$pwcolour = getColourCodeForTarget($pw,$target_pw2_week,$target_pw_week,$target_pw_now);
//appointments
$awcolour = getColourCodeForTarget($aw,$target_aw2_week,$target_aw_week,$target_aw_now);
*/

$jtcolour = 'blue';
//points
$ptcolour = 'blue';
//appointments
$atcolour = 'blue';

//week
//job
$jwcolour = 'blue';
//points
$pwcolour = 'blue';
//appointments
$awcolour = 'blue';



$current_week = (INT)date('W');
$date_today = date("d.m.y");

$arr = array(
	/*"apps" => array(
		"today" => array("figure"=>floor($at),"colour"=>$atcolour) ,		
		"nextday" => array("figure"=>floor($an),"colour"=>$atcolour) ,
		"secondday" => array("figure"=>floor($as),"colour"=>$atcolour) ,
		"thirdday" => array("figure"=>floor($ath),"colour"=>$atcolour) ,
		"week" => array("figure"=>floor($aw),"colour"=>$awcolour)
	),
	"jobs" => array(
		"today" => array("figure"=>floor($jt),"colour"=>$jtcolour) ,		
		"nextday" => array("figure"=>floor($jn),"colour"=>$atcolour) ,
		"secondday" => array("figure"=>floor($js),"colour"=>$atcolour) ,
		"thirdday" => array("figure"=>floor($jth),"colour"=>$atcolour) ,
		"week" => array("figure"=>floor($jw),"colour"=>$jwcolour)
	),*/
	"points" => array(
		"today" => array("figure"=>floor($pt),"colour"=>$ptcolour) ,	
		"todayWWM" => array("figure"=>floor($ptWWM),"colour"=>$ptcolour) ,	
		"todayIB" => array("figure"=>floor($ptIB),"colour"=>$ptcolour) ,	
		"todaySS" => array("figure"=>floor($ptSS),"colour"=>$ptcolour) ,	
		"todayMD" => array("figure"=>floor($ptMD),"colour"=>$ptcolour) ,	
		"nextday" => array("figure"=>floor($pn),"colour"=>$atcolour) ,			
		"nextdayWWM" => array("figure"=>floor($pnWWM),"colour"=>$atcolour) ,	
		"nextdayIB" => array("figure"=>floor($pnIB),"colour"=>$atcolour) ,	
		"nextdaySS" => array("figure"=>floor($pnSS),"colour"=>$atcolour) ,	
		"nextdayMD" => array("figure"=>floor($pnMD),"colour"=>$atcolour) ,
		"secondday" => array("figure"=>floor($ps),"colour"=>$atcolour) ,		
		"seconddayWWD" => array("figure"=>floor($psWWM),"colour"=>$atcolour) ,		
		"seconddayIB" => array("figure"=>floor($psIB),"colour"=>$atcolour) ,
		"seconddaySS" => array("figure"=>floor($psSS),"colour"=>$atcolour) ,
		"seconddayMD" => array("figure"=>floor($psMD),"colour"=>$atcolour) ,
		"thirdday" => array("figure"=>floor($pth),"colour"=>$atcolour) ,
		"thirddayWWM" => array("figure"=>floor($pthWWM),"colour"=>$atcolour) ,
		"thirddayIB" => array("figure"=>floor($pthIB),"colour"=>$atcolour) ,
		"thirddaySS" => array("figure"=>floor($pthSS),"colour"=>$atcolour) ,
		"thirddayMD" => array("figure"=>floor($pthMD),"colour"=>$atcolour) ,		
		"fourthday" => array("figure"=>floor($pfo),"colour"=>$atcolour) ,
		"fourthdayWWM" => array("figure"=>floor($pfoWWM),"colour"=>$atcolour) ,
		"fourthdayIB" => array("figure"=>floor($pfoIB),"colour"=>$atcolour) ,
		"fourthdaySS" => array("figure"=>floor($pfoSS),"colour"=>$atcolour) ,
		"fourthdayMD" => array("figure"=>floor($pfoMD),"colour"=>$atcolour) 
	)
);

echo json_encode($arr);
} else {
	header("Location: http://hclremote:1280/hydroboard/call_centreTwo.php");
	die();
}
?>
