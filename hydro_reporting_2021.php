<?php
	session_start();
	session_unset();
	
/*	echo "Session ID: ".session_id()."</br>";
	Print_r ($_SESSION);	*/

	// Connect to server and select databse.
	include("config.php");
	
/* Config Script START */

	$current_year = 2021;

/* Config Script END */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Board Reports - Yearly Summary <?php echo $current_year ?></title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" src="calendar_db.js"></script>
</head>
<body>

<?php

	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
	$last_week = $current_week - 1;
	$days[0] = 'Saturday';
	$days[1] = 'Sunday';
	$days[2] = 'Monday';
	$days[3] = 'Tuesday';
	$days[4] = 'Wednesday';
	$days[5] = 'Thursday';
	$days[6] = 'Friday';
	$time_now = date ('H:i:s');

	function getAppsHour($hour_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && HOUR(booked_time) = '$hour_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAppsDay($day_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && booked_date = '$day_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAppsDayNumber($day_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && DAYOFWEEK(booked_date) = '$day_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAppsWeek($week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAgentAppsDay($agent_search,$day_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && booked_date = '$day_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAgentAppsWeek($agent_search,$week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAgentJobsWeek($agent_search,$week_search,$year_search) {
		$result = mysql_query("SELECT SUM(job_value) FROM jobs WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$row = mysql_fetch_array($result);
		return $row[0];
	}

	function getAgentPointsWeek($agent_search,$week_search,$year_search) {
		$result = mysql_query("SELECT SUM(points_value) FROM jobs WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$row = mysql_fetch_array($result);
		return $row[0];
	}

	$result = mysql_query("SELECT booked_date FROM jobs WHERE YEAR(booked_date) = $current_year && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date)");
	$numrows = mysql_num_rows($result);
?>

<div id="container">

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Board Reports - Yearly Summary <?php echo $current_year ?></h3>
	</div>

	<div align="center">
		<form class="menu">
			<?php include("reports_menu.php"); ?>
		</form>
	</div>

	<div id="message">
		<?php echo $message.'Year: '.$current_year.', Number of Weeks: '.$numrows;?>
	</div>

	<div id="reports">
	<table align="center">
	<tr>
	<td valign="top">
		<p align="center">Summary of job booking times by hour</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Hour</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	$hour = 9;
	$i = 0;
	while ($hour < 18){
	 	$hourresult = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && HOUR(booked_time) = $hour && Cancelled != 1 && Removed != 1");
		$row = mysql_fetch_array($hourresult);
		$num_per_hour = mysql_numrows($hourresult);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $hour; ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo getAppsHour($hour,$current_year); ?></td>
			</tr>

<?php 

	  $hour++;
	}
?>

		</table>
	</td>
	<td valign="top">
		<p align="center">Summary of job booking times by day</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Day</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	$day = 2;
	$i = 0;
	while ($day < 7){
		$dayresult = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && DAYOFWEEK(booked_date) = $day && Cancelled != 1 && Removed != 1");
		$row = mysql_fetch_array($dayresult);
		$num_per_hour = mysql_numrows($dayresult);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $days[$day] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo getAppsDayNumber($day,$current_year); ?></td>
			</tr>
<?php

	$day++;
	}
?>

		</table>
	</td>
	<td valign="top">
		<p align="center">Summary of weekly results</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 104px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Week</th>
				<th>Starting</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>
<?php

	$weeklyresult = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date) ORDER BY booked_date DESC");
	$i = 0;
	while ($row = mysql_fetch_array($weeklyresult)){
		$appsweek = $row['booked_date'];
		$weeknum = date('W', strtotime($appsweek));
		$starting = date('d/m/Y', strtotime($appsweek));
		$number_of_apps = getAppsWeek($weeknum,$current_year);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $weeknum ?></td>
				<td><?php echo $starting ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>
<?php

	}
?>

		</table>
	</td>
	</tr>
	</table>
	<table align="center">
	<tr>
	<td valign="top">

<?php

	$agentresult = mysql_query("SELECT DISTINCT agent_name FROM jobs WHERE YEAR(booked_date) = '$current_year' && Cancelled != 1 && Removed != 1 ORDER BY agent_name ASC");
	while ($row = mysql_fetch_array($agentresult)){
		$agentname = $row['agent_name'];
?>
		<p align="center"><?php echo $agentname ?></p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 104px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Week</th>
				<th>Starting</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	$agentresult2 = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && agent_name = '$agentname' && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date) ORDER BY booked_date DESC");
	$i = 0;
	while ($row = mysql_fetch_array($agentresult2)){
		$appsweek = $row['booked_date'];
		$weeknum = date('W', strtotime($appsweek));
		$starting = date('d/m/Y', strtotime($appsweek));
		$number_of_apps = getAgentAppsWeek($agentname,$weeknum,$current_year);
		$i++;
?>

		<tr class="tr<?php echo ($i & 1) ?>">
			<td><?php echo $weeknum ?></td>
			<td><?php echo $starting ?></td>
			<td><?php echo $row['SUM(job_value)'] ?></td>
			<td><?php echo $row['SUM(points_value)'] ?></td>
			<td><?php echo $number_of_apps ?></td>
		</tr>

<?php

	}
?>

		</table>

<?php

	}
?>
	</td>
	<td width="100px">
	</td>
	<td valign="top">
<?php

	$weeknumresult = mysql_query("SELECT booked_date FROM jobs WHERE YEAR(booked_date) = $current_year && Cancelled != 1 && Removed != 1 GROUP BY WEEKOFYEAR(booked_date) ORDER BY booked_date DESC");
	while ($row = mysql_fetch_array($weeknumresult)){
		$appsweek = $row['booked_date'];
		$weeknum = date('W', strtotime($appsweek));
		$starting = date('d/m/Y', strtotime($appsweek));
?>

		<p align="center">Week No. <?php echo $weeknum; ?>, Starting: <?php echo $starting; ?></p>
		<table cellpadding=10 class="tbljobs">	
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Day</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	$weeknumresult2 = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $weeknum && Cancelled != 1 && Removed != 1 GROUP BY booked_date ORDER BY booked_date ASC");
	$i = 0;
	while ($row = mysql_fetch_array($weeknumresult2)){
		$number_of_apps = getAppsDay($row['booked_date'],$current_year);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>"> 
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>

<?php

	}

	$weeknumresult3 = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $weeknum && Cancelled != 1 && Removed != 1");
	$row = mysql_fetch_array($weeknumresult3);
	$number_of_apps = getAppsWeek($weeknum,$current_year);
?>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<td>Total</td>
				<td><?php echo $row['SUM(job_value)']; ?></td>
				<td><?php echo $row['SUM(points_value)']; ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
		</table>
		<br/>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Agent</th>
				<th>Jobs</th>
				<th>Figures(�)</th>
				<th>Apps</th>
			</tr>

<?php

	include("config.php");

	$agentsquery = "CREATE TEMPORARY TABLE LeaderBoard (
		`agent_name` varchar(20) NOT NULL,
		`job_number` int(5) NOT NULL,
		`job_value` decimal(3,1) NOT NULL,
		`points_value` decimal(8,2) NOT NULL
	);";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`, `job_number`, `job_value`, `points_value`) SELECT agent_name, job_number, job_value, points_value FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $weeknum && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`) SELECT DISTINCT agent_name FROM apps WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $weeknum && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "SELECT agent_name, SUM(job_value), SUM(points_value) FROM leaderboard GROUP BY agent_name ORDER BY SUM(points_value) DESC;";
	$agentsquery .= "DROP TABLE LeaderBoard";

	mysqli_multi_query($link, $agentsquery) or die("MySQL Error: " . mysqli_error($link) . "<hr>\nQuery: $agentsquery");
	mysqli_next_result($link);
	mysqli_next_result($link);
	mysqli_next_result($link);
	
	if ($result = mysqli_store_result($link)) {
		$i = 0;
		while ($row = mysqli_fetch_array($result)){
			$number_of_apps = getAgentAppsWeek($row['agent_name'],$weeknum,$current_year);
			$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>

<?php

		}
	}
?>

		</table>

<?php

	}
?>

	</td>
	</tr>
	</table>
	</div>
</div>
<br/>

<?php

	mysqli_close($link);
	mysql_close($con);
?>
</body>
</html>