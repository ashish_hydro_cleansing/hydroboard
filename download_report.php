<?php 

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * author : Reka Sambath
 */
    session_start();
    session_unset();

    include("config.php");
    
    
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=report.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

  if($_GET['type'] == "CustomerCarefromtodate"){
	$fromDateCC = $_POST['fromDate'] ; 
	$toDateCC = $_POST['toDate'] ; 
	fputcsv($output, array('Agent Name', 'Category One', 'Category Two', 'Category Three', 'Department', 'DateTime','Call Complete'));                        
                                        $query = "SELECT AgentName, c1.Clas1Name, C2.Clas2Name, C3.Clas3Name, D.DepartmentName, call_date, CallSuccess FROM customercarecalls ccc LEFT JOIN csclas_1 c1 ON c1.Clas1_ID = ccc.Cat1 LEFT JOIN csclas_2 c2 ON Clas2_ID = ccc.cat2 LEFT JOIN csclas_3 c3 ON Clas3_ID = ccc.cat3 LEFT JOIN Department d ON d.DepartmentID = ccc.DepartmentID where date_format(call_date, '%d/%m/%Y') BETWEEN '$fromDateCC' AND '$toDateCC'";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);                       
                                        } 
  }
    if($_GET['type'] == "fromtodate"){
          $fromDate = $_POST['fromDate'] ;        
          $fromDate =  implode('-', array_reverse(explode('/', $fromDate)));
           $toDate = $_POST['toDate'] ; 
         $toDate = implode('-', array_reverse(explode('/', $toDate)));
            if(!isset($_POST['fromDate'])){
                         $fromDate = null;
                    }
            if(!isset($_POST['toDate'])){
                          $toDate = null;
                    }  
            if(!isset($_POST['radioboard'])){
                    $radioboard = null;
            }
                    if($_POST['selectDate'] == "Booked Date"){
                                if($_POST['radioboard'] == "Accounts"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points','Booked Date', 'Booked Time', 'Job Date'));                        
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);                       
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time','Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                            $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                                            $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            }
                                            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                            $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                            $rows = mysql_query($query);
                                            if($rows){
                                                while ($row = mysql_fetch_assoc($rows)) {
                                                        fputcsv($output, $row);
                                                } 
                                        }                         
                                    }

                            }

                            if($_POST['radioboard'] =="Telesales"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Time', 'Booked Date', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date',' Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        }
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Time','Booked Date', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ),ap.booked_time,  date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 

                                    }
                            }

                            if($_POST['radioboard'] == "Roadsweeper"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        }

                                    }
                                }

                            if($_POST['radioboard'] == "Cctwo"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                            }

                            if(($_POST['radioboard'] == "Allboard") || (!isset($_POST['radioboard']))) {
                                if($_POST['selectValue'] == "Jobs"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where booked_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);                       
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date','Booked Time', 'Appt Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time,  date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where booked_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date','Booked Time', 'Job Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where booked_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date','Booked Time', 'Appt Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time,  date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where booked_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                        } 
                                    }
                                }            
                            } 

                            if($_POST['radioboard'] == "CancelledJobs"){
                                if($_POST['selectValue'] == "Jobs"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points','Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value,date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);                       
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time','Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points','Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time','Appt Date','Cancelled Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                        } 
                                    }
                                }            
                            }
                    }
                    
                    else{
                        if($_POST['radioboard'] == "Accounts"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));                        
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);                       
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                            $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                                            $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            }
                                            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date','Booked Time', 'Appt Date' ));
                                            $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                            $rows = mysql_query($query);
                                            if($rows){
                                                while ($row = mysql_fetch_assoc($rows)) {
                                                        fputcsv($output, $row);
                                                } 
                                        }                         
                                    }

                            }

                            if($_POST['radioboard'] =="Telesales"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date','Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date','Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        }
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date','Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 

                                    }
                            }

                            if($_POST['radioboard'] == "Roadsweeper"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 3 ANDap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date','Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        }

                                    }
                                }

                            if($_POST['radioboard'] == "Cctwo"){
                                    if($_POST['selectValue'] == "Jobs"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date','Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                    }
                                    elseif($_POST['selectValue'] == "Appointments"){
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                                        $rows = mysql_query($query);
                                        while ($row = mysql_fetch_assoc($rows)) {
                                                fputcsv($output, $row);
                                        } 
                                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date BETWEEN '$fromDate' AND '$toDate' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                                        $rows = mysql_query($query);
                                        if($rows){
                                            while ($row = mysql_fetch_assoc($rows)) {
                                                    fputcsv($output, $row);
                                            } 
                                        } 
                                    }
                            }

                            if(($_POST['radioboard'] == "Allboard") || (!isset($_POST['radioboard']))) {
                                if($_POST['selectValue'] == "Jobs"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value , date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where job_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);                       
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where app_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value,date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where job_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where app_date BETWEEN '$fromDate' AND '$toDate' AND Cancelled = 0 AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                        } 
                                    }
                                }            
                            } 

                            if($_POST['radioboard'] == "CancelledJobs"){
                                if($_POST['selectValue'] == "Jobs"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date','Booked Time', 'Job Date' ,'Cancelled Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);                       
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time','Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    } 
                                }
                                elseif($_POST['selectValue'] == "Jobs and Appointments"){
                                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points','Booked Date', 'Booked Time', 'Job Date','Cancelled Date'));
                                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time,  date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where cancelled_date BETWEEN '$fromDate' AND '$toDate'  AND Removed = 0";
                                    $rows = mysql_query($query);
                                    while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                    } 
                                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time','Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where cancelled_date BETWEEN '$fromDate' AND '$toDate' AND Removed = 0";
                                    $rows = mysql_query($query);
                                    if($rows){
                                        while ($row = mysql_fetch_assoc($rows)) {
                                            fputcsv($output, $row);
                                        } 
                                    }
                                }            
                            }
                    }
                            
    }
    
    if($_GET['type'] == "bookeddate"){
        if(!isset($_POST['datebooked'])){
                         $datebooked = null;
                    }
        $datebooked = $_POST['datebooked'];     
          $datebooked =  implode('-', array_reverse(explode('/', $datebooked)));
         
        if(!isset($_POST['radioboard'])){
                    $radioboard = null;
        }
        
        if($_POST['radioboard'] == "Accounts"){
                if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));                        
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);                       
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed= 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                        } 
                    }
                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                            $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed= 0";
                            $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            }
                            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                            $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed= 0";
                            $rows = mysql_query($query);
                            if($rows){
                                while ($row = mysql_fetch_assoc($rows)) {
                                        fputcsv($output, $row);
                                } 
                        }                         
                    }
            }
            
        if($_POST['radioboard'] == "Telesales"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed= 0";          
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        }
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed= 0";          
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 

                    }
        }
        
        if($_POST['radioboard'] == "Roadsweeper"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date','Booked Time',  'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ),ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed= 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed= 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    }
        }
        
        if($_POST['radioboard'] == "Cctwo"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ),j.booked_time,  date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed= 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.booked_date = '$datebooked' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed= 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.booked_date = '$datebooked' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed= 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 

                }
        }
        
        if(($_POST['radioboard'] == "Allboard") || (!isset($_POST['radioboard']))){
            if($_POST['selectValue'] == "Jobs"){
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Job date', 'Booked Date', 'Booked Time'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( job_date , '%d/%m/%Y' ), date_format( booked_date , '%d/%m/%Y' ), booked_time FROM jobs where booked_date = '$datebooked' AND Cancelled = 0 AND Removed= 0";
                    $rows = mysql_query($query);
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                }
            elseif($_POST['selectValue'] == "Appointments"){
                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time','Appt Date', 'Booked Date','Booked Time'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( app_date , '%d/%m/%Y' ), date_format( booked_date , '%d/%m/%Y' ), booked_time FROM apps where booked_date ='$datebooked' AND Cancelled = 0 AND Removed= 0";
                    $rows = mysql_query($query);
                    if($rows){
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                    }
                }
            elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Job date', 'Booked Date','Booked Time'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( job_date , '%d/%m/%Y' ), date_format( booked_date , '%d/%m/%Y' ), booked_time FROM jobs where booked_date = '$datebooked' AND Cancelled = 0 AND Removed= 0";
                    $rows = mysql_query($query);
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }
                        
                    fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time','Appt Date', 'Booked Date', 'Booked Time'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( app_date , '%d/%m/%Y' ), date_format( booked_date , '%d/%m/%Y' ), booked_time FROM apps where booked_date ='$datebooked' AND Cancelled = 0 AND Removed= 0";
                    $rows = mysql_query($query);
                    if($rows){
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                    }
            } 
            
        }
    }
    
    if($_GET['type'] == "jobdate"){
        if(!isset($_POST['dateofjob'])){
                         $dateofjob = null;
                    }
        if(!isset($_POST['radioboard'])){
                    $radioboard = null;
        }
                $dateofjob = $_POST['dateofjob']; 
          $dateofjob =  implode('-', array_reverse(explode('/', $dateofjob)));
               
        if($_POST['radioboard'] == "Accounts"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));                        
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);                       
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative','Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                        } 
                    }
                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                            $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 1 AND j.Cancelled = 0 AND j.Removed = 0";
                            $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            }
                            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                            $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 1 AND ap.Cancelled = 0 AND ap.Removed = 0";
                            $rows = mysql_query($query);
                            if($rows){
                                while ($row = mysql_fetch_assoc($rows)) {
                                        fputcsv($output, $row);
                                } 
                        }                         
                    }
        }
        
        if($_POST['radioboard'] == "Telesales"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif($_POST['selectValue'] == "Jobs and Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 2 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        }
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name, ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 2 AND ap.Cancelled = 0 AND ap.Removed = 0";          
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 

                    }
        }
        
        if($_POST['radioboard'] == "Roadsweeper"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed = 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 3 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name','Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 3 AND ap.Cancelled = 0 AND ap.Removed = 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }

                    }
        }
        
        if($_POST['radiobord'] == "Cctwo"){
                    if($_POST['selectValue'] == "Jobs"){
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                    }
                    elseif($_POST['selectValue'] == "Appointments"){
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$dateofjob' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        } 
                    }
                    elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                        fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                        $query = "SELECT j.agent_name, j.job_number, j.job_type, j.job_value, j.company_name, j.points_value, date_format( j.booked_date , '%d/%m/%Y' ), j.booked_time, date_format( j.job_date , '%d/%m/%Y' ) FROM jobs j INNER JOIN agents a where j.job_date = '$dateofjob' AND a.agent_name = j.agent_name AND a.role_id = 5 AND j.Cancelled = 0 AND j.Removed = 0";
                        $rows = mysql_query($query);
                        while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                        } 
                        fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
                        $query = "SELECT ap.agent_name, ap.job_number, ap.rep_name,ap.company_name, ap.app_postcode, ap.app_time, date_format( ap.booked_date , '%d/%m/%Y' ), ap.booked_time, date_format( ap.app_date , '%d/%m/%Y' ) FROM apps ap INNER JOIN agents a where ap.app_date = '$datofjob' AND a.agent_name = ap.agent_name AND a.role_id = 5 AND ap.Cancelled = 0 AND ap.Removed = 0";
                        $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    }
            
        }
        
        if(($_POST['radioboard'] == "Allboard") || (!isset($_POST['radioboard']))){
            if($_POST['selectValue'] == "Jobs"){
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points',' Booked Date' , 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where job_date = '$dateofjob' AND Cancelled = 0 AND Removed = 0";
                    $rows = mysql_query($query);
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                }
            elseif($_POST['selectValue'] == "Appointments"){ 
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time','Booked Date', 'Booked Time', 'Appt Date' ));
                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where app_date ='$dateofjob' AND Cancelled = 0 AND Removed = 0";
                    $rows = mysql_query($query);
                    if($rows){
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                    }
            }
            elseif ($_POST['selectValue'] == "Jobs and Appointments") {
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points',' Booked Date' , 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where job_date = '$dateofjob' AND Cancelled = 0 AND Removed = 0";
                    $rows = mysql_query($query);
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        } 
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company Name', 'Postcode', 'Appt Time','Booked Date', 'Booked Time', 'Appt Date' ));
                    $query = "SELECT agent_name, job_number, rep_name,company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where app_date ='$dateofjob' AND Cancelled = 0 AND Removed = 0";
                    $rows = mysql_query($query);
                    if($rows){
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                        }            
                    }
            } 
            
        }
    } 

    
    if($_GET['type'] == "agentname"){
      $agent_name = $_POST['selectagentname'];
      if($_POST['selectValue'] == "Jobs"){
            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
            $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where agent_name = '$agent_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
      }
      elseif ($_POST['selectValue'] == "Appointments"){
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where agent_name = '$agent_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
            if($rows){
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
      }
      }
      elseif ($_POST['selectValue'] == "Jobs and Appointments") {
            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
            $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where agent_name = '$agent_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
                while ($row = mysql_fetch_assoc($rows)) {
                        fputcsv($output, $row);
                }
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where agent_name = '$agent_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
            if($rows){
                while ($row = mysql_fetch_assoc($rows)) {
                        fputcsv($output, $row);
                } 
            }      
        }
    else {
            echo 'Sorry, Something went wrong.';
        }
    }
    if($_GET['type'] == "companyname"){
        $company_name = $_POST['company'];
      if($_POST['selectValue'] == "Jobs"){
            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
            $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where company_name = '$company_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
      }
      elseif ($_POST['selectValue'] == "Appointments"){
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where company_name = '$company_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
            if($rows){
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
      }
    }
        elseif($_POST['selectValue'] == "Jobs and Appointments"){
            fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
            $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where company_name = '$company_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
                while ($row = mysql_fetch_assoc($rows)) {
                        fputcsv($output, $row);
                } 
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where company_name = '$company_name' AND Cancelled = 0 AND Removed = 0";
            $rows = mysql_query($query);
                if($rows){
                    while ($row = mysql_fetch_assoc($rows)) {
                            fputcsv($output, $row);
                    } 
                }
        }
        else{
            echo "Sorry,something went wrong";
        }
    }
    if($_GET['type'] == "cancelledjobs"){
        $select_value = $_POST['selectcancelledjobs'];
        $radioValue = $_POST['formradio'];
        if($select_value == "Jobs"){
            $currentdate = date("Y-m-d");
            if($radioValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date = '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$weekdate' AND '$currentdate'  AND Removed = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$monthdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$yeardate' AND '$currentdate' AND Removed = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            else{
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date','Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ),date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
                }
        }
        elseif ($select_value == "Appointments"){
            $currentdate = date("Y-m-d");
            if($radioValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date = '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$weekdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$monthdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radioValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$yeardate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            else{
            
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND Removed = 0";
            $rows = mysql_query($query);
            if($rows){
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
            }
        
            }
        }
        
        elseif($select_value == "Jobs and Appointments"){
            $currentdate = date("Y-m-d");
            if($radioValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date = '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date = '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            elseif ($radioValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                     fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$weekdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$weekdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                            } 
                        }    
            }
            elseif ($radioValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$monthdate' AND '$currentdate' AND Removed = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$monthdate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }    
            }
            elseif ($radioValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND cancelled_date BETWEEN '$yeardate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND cancelled_date BETWEEN '$yeardate' AND '$currentdate' AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            else{
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date','Cancelled Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ),date_format( cancelled_date , '%d/%m/%Y' ) FROM jobs where Cancelled = 1 AND Removed = 0";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Cancelled Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( cancelled_date , '%d/%m/%Y' ) FROM apps where Cancelled = 1 AND Removed = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                fputcsv($output, $row);
                            } 
                        }
                }
        }
        else{
            echo "Sorry,Something went wrong";
        }
    }
    
    if($_GET['type'] == "removedjobs"){
        $select_value = $_POST['selectremovedjobs'];
        $radiotwoValue = $_POST['formtworadio'];        
        if($select_value == "Jobs"){
            $currentdate = date("Y-m-d");
            if($radiotwoValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date = '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radiotwoValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$weekdate' AND '$currentdate' AND Cancelled = 0  ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                        } 
                     }
            }
            elseif ($radiotwoValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$monthdate' AND '$currentdate' AND Cancelled = 0  ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radiotwoValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$yeardate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            else{
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date','Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ),date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
                }
        }
        elseif ($select_value == "Appointments"){
            $currentdate = date("Y-m-d");
            if($radiotwoValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Time', 'Booked Date', 'Appt Date',));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date = '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radiotwoValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$weekdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radiotwoValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$monthdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            elseif ($radiotwoValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$yeardate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                     if($rows){
                     while ($row = mysql_fetch_assoc($rows)) {
                             fputcsv($output, $row);
                     } 
                     }
            }
            else{
            
            fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
            $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND Cancelled = 0 ";
            $rows = mysql_query($query);
            if($rows){
            while ($row = mysql_fetch_assoc($rows)) {
                    fputcsv($output, $row);
            } 
            }
        
            }
    }
        elseif($select_value == "Jobs and Appointments"){
            $currentdate = date("Y-m-d");
            if($radiotwoValue == "Today"){                
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date = '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date',));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date = '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            elseif ($radiotwoValue == "Week") {
                $date = strtotime("-7 day");
                $weekdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$weekdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                               } 
                        }
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$weekdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            elseif ($radiotwoValue == "Month") {
                $date = strtotime("-30 day");
                $monthdate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$monthdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date'));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$monthdate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            elseif ($radiotwoValue == "Year") {
                $date = strtotime("-365 day");
                $yeardate = date('Y-m-d', $date);
                fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date', 'Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND removed_date BETWEEN '$yeardate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
                    $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND removed_date BETWEEN '$yeardate' AND '$currentdate' AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
            }
            else{
                    fputcsv($output, array('Agent', 'Job Number', 'Job Type', 'Jobs', 'Company', 'Points', 'Booked Date', 'Booked Time', 'Job Date','Removed Date'));
                    $query = "SELECT agent_name, job_number, job_type, job_value, company_name, points_value, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( job_date , '%d/%m/%Y' ),date_format( removed_date , '%d/%m/%Y' ) FROM jobs where Removed = 1 AND Cancelled = 0 ";
                    $rows = mysql_query($query);
                        if($rows){
                            while ($row = mysql_fetch_assoc($rows)) {
                                    fputcsv($output, $row);
                            } 
                        }
                    fputcsv($output, array('Agent', 'Job Number', 'Representative', 'Company', 'Postcode', 'Appt Time', 'Booked Date', 'Booked Time', 'Appt Date','Removed Date' ));
                        $query = "SELECT agent_name, job_number, rep_name, company_name, app_postcode, app_time, date_format( booked_date , '%d/%m/%Y' ), booked_time, date_format( app_date , '%d/%m/%Y' ), date_format( removed_date , '%d/%m/%Y' ) FROM apps where Removed = 1 AND Cancelled = 0 ";
                        $rows = mysql_query($query);
                            if($rows){
                                while ($row = mysql_fetch_assoc($rows)) {
                                        fputcsv($output, $row);
                                } 
                            }
                }
        }
    }
    
    
    
            ?>
