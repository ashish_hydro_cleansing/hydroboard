<?php
	session_start();
	session_unset();
	
/*	echo "Session ID: ".session_id()."</br>";
	Print_r ($_SESSION);	*/
	
	include("config.php");
	$time_now = date ('H:i:s');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<title>Hydro-Board Log-In</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" type="text/javascript">

	function checkAgentName() {
		if (document.log_in.agent_log_in.value=="") {
			alert ("Enter Your Name");
			document.log_in.agent_log_in.focus();
		}
		else if (document.log_in.agent_password.value=="") {
			alert ("Enter Your Password");
			document.log_in.agent_password.focus();
		}
		else document.log_in.submit();
	}

	</script>
</head>

<body onLoad="self.focus();document.log_in.agent_log_in.focus()">

<div id="container">

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Board Log-In</h3>
	</div>
	
	<div id="message">
		<?php echo $message ?>
	</div>

	<div id="log_in_box">
		<form name="log_in" method="post" action="checklogin.php">
		<table align="center" cellpadding=4 cellspacing=0>
			<tr bgcolor='#FF6600' style="font-size: 14px; font-weight: bold">
				<th>Log-On</th>
			</tr>
			<colgroup style="width: 120px"></colgroup>
			<tr>
				<td>
					<select name="agent_log_in" id="agent_log_in">
					<option value="">
					<?php
						$result = mysql_query("SELECT agent_name FROM agents WHERE enabled='1' ORDER BY agent_name ASC");
						while ($row = mysql_fetch_array($result)) {
							echo '<option value="'.$row['agent_name'].'"';
							if ($row['agent_name'] == $user) { echo ' SELECTED '; }
							echo '>'.$row['agent_name'].'</option>'.$row['agent_name'];
						}
					?> 
					</select>
					<input name="agent_password" type="password" id="agent_password">
					<input type="submit" name="Submit" value="Login" onClick="checkAgentName();return false">
				</td>
			</tr>
		</table>
		</form>
	</div>
<br>
	<div id="view_reports">
		<table align="center" cellpadding=4 cellspacing=0>
			<tr bgcolor="#FF6600" style="font-size: 14px; font-weight: bold">
				<th colspan="4">View Reports</th>
			</tr>
			<tr>
				<td>
					<form class="menu">
						<?php include("reports_menu.php"); ?>
					</form>
				</td>
			</tr>
		</table>
	</div>

</div>

</body>
</html>