function renderData(rs){
	/*var appsTodayDiv = document.getElementsByClassName("apps day")[0];
	appsTodayDiv.classList.add('orange');
	appsTodayDiv.innerHTML = rs.apps.today.figure;
	
	var appsNextDayDiv = document.getElementsByClassName("apps nextday")[0];
	appsNextDayDiv.classList.add('orange');
	appsNextDayDiv.innerHTML = rs.apps.nextday.figure;
	
	var appsSecondDayDiv = document.getElementsByClassName("apps secondday")[0];
	appsSecondDayDiv.classList.add('orange');
	appsSecondDayDiv.innerHTML = rs.apps.secondday.figure;
	
	var appsThirdDayDiv = document.getElementsByClassName("apps thirdday")[0];
	appsThirdDayDiv.classList.add('orange');
	appsThirdDayDiv.innerHTML = rs.apps.thirdday.figure; */
	
	var pointsTodayDiv = document.getElementsByClassName("points day")[0];
	pointsTodayDiv.classList.add('orange');
	pointsTodayDiv.innerHTML = '�' + rs.points.today.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsTodayWWMDiv = document.getElementsByClassName("pointsWWM day")[0];
	pointsTodayWWMDiv.classList.add('red');
	pointsTodayWWMDiv.innerHTML = '�' + rs.points.todayWWM.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsTodayIBDiv = document.getElementsByClassName("pointsIB day")[0];
	pointsTodayIBDiv.classList.add('blue');
	pointsTodayIBDiv.innerHTML = '�' + rs.points.todayIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsTodaySSDiv = document.getElementsByClassName("pointsSS day")[0];
	pointsTodaySSDiv.classList.add('gold');
	pointsTodaySSDiv.innerHTML = '�' + rs.points.todaySS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsTodayMDDiv = document.getElementsByClassName("pointsMD day")[0];
	pointsTodayMDDiv.classList.add('green');
	pointsTodayMDDiv.innerHTML = '�' + rs.points.todayMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	
	var pointsNextDayDiv = document.getElementsByClassName("points nextday")[0];
	pointsNextDayDiv.classList.add('orange');
	pointsNextDayDiv.innerHTML = '�' + rs.points.nextday.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsNextDayWWMDiv = document.getElementsByClassName("pointsWWM nextday")[0];
	pointsNextDayWWMDiv.classList.add('red');
	pointsNextDayWWMDiv.innerHTML = '�' + rs.points.nextdayWWM.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsNextDayIBDiv = document.getElementsByClassName("pointsIB nextday")[0];
	pointsNextDayIBDiv.classList.add('blue');
	pointsNextDayIBDiv.innerHTML = '�' + rs.points.nextdayIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsNextDaySSDiv = document.getElementsByClassName("pointsSS nextday")[0];
	pointsNextDaySSDiv.classList.add('gold');
	pointsNextDaySSDiv.innerHTML = '�' + rs.points.nextdaySS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	var pointsNextDayMDDiv = document.getElementsByClassName("pointsMD nextday")[0];
	pointsNextDayMDDiv.classList.add('green');
	pointsNextDayMDDiv.innerHTML = '�' + rs.points.nextdayMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	
	var pointsSecondDayDiv = document.getElementsByClassName("points secondday")[0];
	pointsSecondDayDiv.classList.add('orange');
	pointsSecondDayDiv.innerHTML = '�' + rs.points.secondday.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsSecondDayWWMDiv = document.getElementsByClassName("pointsWWM secondday")[0];
	pointsSecondDayWWMDiv.classList.add('red');
	pointsSecondDayWWMDiv.innerHTML = '�' + rs.points.seconddayWWD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsSecondDayIBDiv = document.getElementsByClassName("pointsIB secondday")[0];
	pointsSecondDayIBDiv.classList.add('blue');
	pointsSecondDayIBDiv.innerHTML = '�' + rs.points.seconddayIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsSecondDaySSDiv = document.getElementsByClassName("pointsSS secondday")[0];
	pointsSecondDaySSDiv.classList.add('gold');
	pointsSecondDaySSDiv.innerHTML = '�' + rs.points.seconddaySS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsSecondDayMDDiv = document.getElementsByClassName("pointsMD secondday")[0];
	pointsSecondDayMDDiv.classList.add('green');
	pointsSecondDayMDDiv.innerHTML = '�' + rs.points.seconddayMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	
	var pointsThirdDayDiv = document.getElementsByClassName("points thirdday")[0];
	pointsThirdDayDiv.classList.add('orange');
	pointsThirdDayDiv.innerHTML = '�' + rs.points.thirdday.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsThirdDayWWMDiv = document.getElementsByClassName("pointsWWM thirdday")[0];
	pointsThirdDayWWMDiv.classList.add('red');
	pointsThirdDayWWMDiv.innerHTML = '�' + rs.points.thirddayWWM.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsThirdDayIBDiv = document.getElementsByClassName("pointsIB thirdday")[0];
	pointsThirdDayIBDiv.classList.add('blue');
	pointsThirdDayIBDiv.innerHTML = '�' + rs.points.thirddayIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsThirdDaySSDiv = document.getElementsByClassName("pointsSS thirdday")[0];
	pointsThirdDaySSDiv.classList.add('gold');
	pointsThirdDaySSDiv.innerHTML = '�' + rs.points.thirddaySS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsThirdDayMDDiv = document.getElementsByClassName("pointsMD thirdday")[0];
	pointsThirdDayMDDiv.classList.add('green');
	pointsThirdDayMDDiv.innerHTML = '�' + rs.points.thirddayMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	
	var pointsFourthDayDiv = document.getElementsByClassName("points fourthday")[0];
	pointsFourthDayDiv.classList.add('orange');
	pointsFourthDayDiv.innerHTML = '�' + rs.points.fourthday.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsFourthDayWWMDiv = document.getElementsByClassName("pointsWWM fourthday")[0];
	pointsFourthDayWWMDiv.classList.add('red');
	pointsFourthDayWWMDiv.innerHTML = '�' + rs.points.fourthdayWWM.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsFourthDayIBDiv = document.getElementsByClassName("pointsIB fourthday")[0];
	pointsFourthDayIBDiv.classList.add('blue');
	pointsFourthDayIBDiv.innerHTML = '�' + rs.points.fourthdayIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsFourthDaySSDiv = document.getElementsByClassName("pointsSS fourthday")[0];
	pointsFourthDaySSDiv.classList.add('gold');
	pointsFourthDaySSDiv.innerHTML = '�' + rs.points.fourthdaySS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsFourthDayMDDiv = document.getElementsByClassName("pointsMD fourthday")[0];
	pointsFourthDayMDDiv.classList.add('green');
	pointsFourthDayMDDiv.innerHTML = '�' + rs.points.fourthdayMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
	
	/*var jobsTodayDiv = document.getElementsByClassName("jobs day")[0]; 
	jobsTodayDiv.classList.add('orange');
	jobsTodayDiv.innerHTML = rs.jobs.today.figure;
	
	var jobsNextDayDiv = document.getElementsByClassName("jobs nextday")[0];
	jobsNextDayDiv.classList.add('orange');
	jobsNextDayDiv.innerHTML = rs.jobs.nextday.figure;
	
	var jobsSecondDayDiv = document.getElementsByClassName("jobs secondday")[0];
	jobsSecondDayDiv.classList.add('orange');
	jobsSecondDayDiv.innerHTML = rs.jobs.secondday.figure;
	
	var jobsThirdDayDiv = document.getElementsByClassName("jobs thirdday")[0];
	jobsThirdDayDiv.classList.add('orange');
	jobsThirdDayDiv.innerHTML = rs.jobs.thirdday.figure;;
	
	var appsWeekDiv = document.getElementsByClassName("apps week")[0];
	appsWeekDiv.classList.add('orange');
	appsWeekDiv.innerHTML = rs.apps.week.figure;*/
	
	var pointsWeekDiv = document.getElementsByClassName("points week")[0];
	pointsWeekDiv.classList.add('orange');
	pointsWeekDiv.innerHTML = '�' + rs.points.week.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsWeekWWMDiv = document.getElementsByClassName("pointsWWM week")[0];
	pointsWeekWWMDiv.classList.add('red');
	pointsWeekWWMDiv.innerHTML = '�' + rs.points.weekWWM.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsWeekIBDiv = document.getElementsByClassName("pointsIB week")[0];
	pointsWeekIBDiv.classList.add('blue');
	pointsWeekIBDiv.innerHTML = '�' + rs.points.weekIB.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsWeekSSDiv = document.getElementsByClassName("pointsSS week")[0];
	pointsWeekSSDiv.classList.add('gold');
	pointsWeekSSDiv.innerHTML = '�' + rs.points.weekSS.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	var pointsWeekMDDiv = document.getElementsByClassName("pointsMD week")[0];
	pointsWeekMDDiv.classList.add('green');
	pointsWeekMDDiv.innerHTML = '�' + rs.points.weekMD.figure.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	
	/*var jobsWeekDiv = document.getElementsByClassName("jobs week")[0];
	jobsWeekDiv.classList.add('orange');
	jobsWeekDiv.innerHTML = rs.jobs.week.figure;*/
	
	setTimeout(function(){longPollData()},1000);
}
function setPaddingLeft(x) {
	if(x < 10){
		x = 15;
	}
	document.getElementById("column-headings").style.paddingLeft = x + "px";
	var t = document.getElementById("table");
	document.getElementById("tableTop").style.left = x + "px";
	var rows = t.getElementsByClassName("row");
	for (var i = 0; i < rows.length; i++){
		var ele = rows[i].children[rows[i].children.length - 1];
		ele.style.paddingLeft = x + "px";
	}
}
function setTable() {
	var iw = window.innerWidth;
	var tw = document.getElementById("tableTop").offsetWidth;
	var os = (iw - tw) / 2;
	setPaddingLeft(os);
}
function longPollData(){
	setTimeout(function(){
		loadData();
	},1000);
}			