<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Board Reports - Today/This Week</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" src="calendar_db.js"></script>
	<meta http-equiv="refresh" content="30" >

</head>
<body>

<?php

	include("config.php");

	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
	$current_week = (INT)date('W');
	$current_year = date("Y");
	$last_week = $current_week - 1;
	$last_year = date("Y");
	$time_now = date ('H:i:s');

	if ($last_week == 0) {
			$last_week = 52;
			$last_year --;
	}
	
	function getAppsDay($day_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && booked_date = '$day_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	function getAppsToDay($day_search) {
		$date_today = date("Y-m-d");
		$result = mysql_query("SELECT COUNT(*) as totalApps FROM apps WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_fetch_array($result);
		return $num_apps['totalApps'];
	}
	function getAppsWeek($week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAgentAppsDay($agent_search,$day_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && booked_date = '$day_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}

	function getAgentAppsWeek($agent_search,$week_search,$year_search) {
		$result = mysql_query("SELECT * FROM apps WHERE YEAR(booked_date) = '$year_search' && agent_name = '$agent_search' && WEEKOFYEAR(booked_date) = '$week_search' && Cancelled != 1 && Removed != 1");
		$num_apps = mysql_numrows($result);
		return $num_apps;
	}
	
	function getAgentAppsToDay($agent_search){
		$date_today = date("Y-m-d");
		$result = mysql_query("SELECT COUNT(*) as totalApps FROM apps WHERE booked_date = '$date_today' && agent_name = '$agent_search' && Cancelled != 1 && Removed != 1");$num_apps = mysql_fetch_array($result);
		return $num_apps['totalApps'];
	}
?>

<div id="container">

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Board Reports - This Week/Last Week</h3>
	</div>

	<div align="center">
		<form class="menu">
			<input type="button" class="btn" value="Log-Out" onClick="parent.location='index.php'"> //
			<?php include("reports_menu.php"); ?>
		</form>
	</div>

	<div id="message">
		<?php echo $message.'Date Today: '.$date_today;?>,
		<?php echo $message.'Current Week: '.$current_week;?>,
		<?php echo $message.'Current Year: '.$current_year;?>,
		<?php echo $message.'Last Week: '.$last_week;?>,
		<?php echo $message.'Last Year: '.$last_year;?>
	</div>

	<div id="reports">
	<table align="center">
	<tr>
	<td valign="top">
		<p align="center">This Week's Jobs and Apps</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Day</th>
				<th>Jobs</th>
				<th>Points</th>
				<th>Apps</th>
			</tr>

<?php

	$thisweek = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week && Cancelled != 1 && Removed != 1 GROUP BY booked_date ORDER BY booked_date ASC");
	$i = 0;
	while ($row = mysql_fetch_array($thisweek)){
		$number_of_apps = getAppsDay($row['booked_date'],$current_year);
		$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>

<?php

	}

	$thisweek2 = mysql_query("SELECT SUM(job_value), SUM(points_value) FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week && Cancelled != 1 && Removed != 1");
	$row = mysql_fetch_array($thisweek2);
	$number_of_apps = getAppsWeek($current_week,$current_year);
?>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<td>Total</td>
				<td><?php echo $row['SUM(job_value)']; ?></td>
				<td><?php echo $row['SUM(points_value)']; ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
		</table>
	</td>
	<td valign="top">
		<p align="center">Today's Jobs and Apps</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Day</th>
				<th>Jobs</th>
				<th>Points</th>
				<th>Apps</th>
			</tr>

<?php

	$thisweek = mysql_query("SELECT booked_date, SUM(job_value), SUM(points_value) FROM jobs WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1 GROUP BY booked_date");
	$i = 0;
	while ($row = mysql_fetch_array($thisweek)){
		$number_of_apps = getAppsToDay($row['booked_date']);
		
?>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps ?></td>
			</tr>

<?php
		
	}

?>

		</table>
	</td>
	</tr>
	</table>
	<table align="center">
	<tr>
	<td valign="top">
		<p align="center">This Week's Leaderboards</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Agent</th>
				<th>Jobs</th>
				<th>Points</th>
				<th>Apps</th>
			</tr>

<?php

	include("config.php");

	$agentsquery = "CREATE TEMPORARY TABLE LeaderBoard (
		`agent_name` varchar(20) NOT NULL,
		`job_number` int(5) NOT NULL,
		`job_value` decimal(3,1) NOT NULL,
		`points_value` decimal(8,2) NOT NULL
	);";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`, `job_number`, `job_value`, `points_value`) SELECT agent_name, job_number, job_value, points_value FROM jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "INSERT INTO LeaderBoard (`agent_name`) SELECT DISTINCT agent_name FROM apps WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week && Cancelled != 1 && Removed != 1;";
	$agentsquery .= "SELECT agent_name, SUM(job_value), SUM(points_value) FROM leaderboard GROUP BY agent_name ORDER BY SUM(points_value) DESC;";
	$agentsquery .= "DROP TABLE LeaderBoard";

	mysqli_multi_query($link, $agentsquery) or die("MySQL Error: " . mysqli_error($link) . "<hr>\nQuery: $agentsquery");
	mysqli_next_result($link);
	mysqli_next_result($link);
	mysqli_next_result($link);

	if ($result = mysqli_store_result($link)) {
		$i = 0;
		while ($row = mysqli_fetch_array($result)){
			$number_of_apps = getAgentAppsWeek($row['agent_name'],$current_week,$current_year);
			$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>
			
<?php

		}
	}
?>

		</table>
	</td>
	<td valign="top">
		<p align="center">Today's Leaderboards</p>
		<table cellpadding=10 class="tbljobs">
			<colgroup style="width: 150px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 80px"></colgroup>

			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Agent</th>
				<th>Jobs</th>
				<th>Points</th>
				<th>Apps</th>
			</tr>

<?php

	include("config.php");

	$agentsquery2 = "CREATE TEMPORARY TABLE LeaderBoard (
		`agent_name` varchar(20) NOT NULL,
		`job_number` int(5) NOT NULL,
		`job_value` decimal(3,1) NOT NULL,
		`points_value` decimal(8,2) NOT NULL
	);";
	$agentsquery2 .= "INSERT INTO LeaderBoard (`agent_name`, `job_number`, `job_value`, `points_value`) SELECT agent_name, job_number, job_value, points_value FROM jobs WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1;";
	$agentsquery2 .= "INSERT INTO LeaderBoard (`agent_name`) SELECT DISTINCT agent_name FROM apps WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1;";
	$agentsquery2 .= "SELECT agent_name, SUM(job_value), SUM(points_value) FROM leaderboard GROUP BY agent_name ORDER BY SUM(points_value) DESC;";
	$agentsquery2 .= "DROP TABLE LeaderBoard";

	mysqli_multi_query($link, $agentsquery2) or die("MySQL Error: " . mysqli_error($link) . "<hr>\nQuery: $agentsquery2");
	mysqli_next_result($link);
	mysqli_next_result($link);
	mysqli_next_result($link);

	if ($result = mysqli_store_result($link)) {
		$i = 0;
		while ($row = mysqli_fetch_array($result)){
			$number_of_apps = getAgentAppsToDay($row['agent_name']);
			$i++;
?>

			<tr class="tr<?php echo ($i & 1) ?>">
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['SUM(job_value)'] ?></td>
				<td><?php echo $row['SUM(points_value)'] ?></td>
				<td><?php echo $number_of_apps; ?></td>
			</tr>

<?php

		}
	}
?>

		</table>
	</td>
	</tr>
	</table>
	</div>
</div>
<br/>

<?php

	mysqli_close($link);
	mysql_close($con);
?>
</body>
</html>