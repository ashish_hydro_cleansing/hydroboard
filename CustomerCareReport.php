<?php
session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
	
include("config.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Report Generation Form</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/south-street/jquery-ui.css" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
        <script>
            $(function() {
              $( "#from" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#to" ).datepicker({
                dateFormat: 'dd/mm/yy', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                }
              });
            });
            
        </script>
    </head>
    <body>
        <div class="container">
            <h2><span class="label label-primary">Customer Care Services Report</span></h2>
            <br><br> 
            <div id="fromtodate" class="reports">
                <form role="form" action="download_report.php?type=CustomerCarefromtodate" method="post">
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="from">From:</label>
                        <input type="text" class="form-control" id="from" name="fromDate" required>
                    </div>
                    <div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="to">To:</label>
                        <input type="text" class="form-control" id="to" name="toDate" required>
                    </div>  
					<div class="form-group col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary" name="generate" id="generate_report">Generate Report</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                </form>
            </div>
            
        </div>
    </body>
</html>


             
            