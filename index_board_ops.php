<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Soft-Board : Job details for next 30 days</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
    <meta http-equiv="refresh" content="30" >
</head>
<body>

<?php

	include("msconfig.php");

?>

<div id="container">
	<p style="margin: 5px 0 0 0; padding: 0; font-size: 30px; text-align: center; font-weight: bold">Job details for next 30 days</p>

<table align="center">
	<tr>
        <th>Job No.</th>
        <th>Date</th>
        <th>Time</th>
        <th>Status</th>
        <th>Description</th>
        <th>Vehicle</th>
        <th>Client</th>
        <th>Address</th>
        <th>Contact Name</th>
        <th>Contact Number</th>
	</tr>
    
<?php
	$query = mssql_query("SELECT JobID, Description, WorkDate, WorkStartTime, Status, tblJobs.ClientID, tblClient.ClientName, Tanker, AddressLine1, AddressLine2, AddressLine3, AddressLine4, Area, Postcode, SiteContactNumber, SiteContactName, Jetter, CombiUnit, SuperCombi FROM tblJobs LEFT JOIN tblClient ON tblJobs.ClientID = tblClient.ClientID WHERE (WorkDate >= CONVERT(date, GETDATE())) AND (WorkDate <= CONVERT(date, DATEADD(dd, 30, GETDATE()))) AND ((Status = 'Pending') OR (Status = 'Awaiting Information')) ORDER BY WorkDate");
	while ($row = mssql_fetch_array($query)){
		$i++;
		$date = date("jS F", strtotime($row['WorkDate']));
		$time = substr($row['WorkStartTime'], -8);
		if ($row['Tanker'] == 1) { $tanker = 'Tanker'; } else { $tanker = NULL; }
		if ($row['Jetter'] == 1) { $jetter = 'Jetter'; } else { $jetter = NULL; }
		if ($row['CombiUnit'] == 1) { $combiunit = 'Combi Unit'; } else { $combiunit = NULL; }
		if ($row['SuperCombi'] == 1) { $supercombi = 'Super Combi'; } else { $supercombi = NULL; }
		$vehicle = "{$tanker} {$jetter} {$combiunit} {$supercombi}";
		$address = "{$row['AddressLine1']} {$row['AddressLine2']} {$row['AddressLine3']} {$row['AddressLine4']} {$row['Area']} {$row['Postcode']}";
?>
    
	<tr class="tr<?php echo ($i & 1) ?>">
        <td><?php echo $row['JobID']; ?></td>
        <td><?php echo $date; ?></td>
        <td><?php echo $time; ?></td>
        <td><?php echo $row['Status']; ?></td>
        <td><?php echo $row['Description']; ?></td>
        <td><?php echo $vehicle; ?></td>
        <td><?php echo $row['ClientName']; ?></td>
        <td><?php echo $address; ?></td>
        <td><?php echo $row['SiteContactName']; ?></td>
        <td><?php echo $row['SiteContactNumber']; ?></td>
	</tr>
<?php } ?>
</table>
</div>
<br/>

<?php

	mssql_free_result($query);
	mssql_close($con);
?>
</body>
</html>