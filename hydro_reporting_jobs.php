<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Hydro-Board Reports - This Week/Last Week</title>
	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" src="calendar_db.js"></script>
</head>
<body>

<?php

	include("msconfig.php");

	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);
	$current_week = (INT)date('W');
	$current_year = date("Y");
	$time_now = date('H:i:s');

?>

<div id="container">

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Soft Reports - Jobs Pending This Week</h3>
	</div>

	<div align="center">
		<form class="menu">
			<?php include("reports_menu.php"); ?>
		</form>
	</div>

	<div id="message">
		<?php echo $message.'Date Today: '.$date_today;?>,
		<?php echo $message.'Current Week: '.$current_week;?>,
		<?php echo $message.'Current Year: '.$current_year;?>,
	</div>

	<div id="reports">
	<table align="center">
	<tr>
	<td align="center" valign="top">
	  <p align="center">Jobs Pending Today</p>
	  <table cellpadding=10 class="tbljobs">
	    <colgroup style="width: 150px"></colgroup>
	    <colgroup style="width: 100px"></colgroup>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <th>Agent</th>
	      <th>Jobs</th>
	    </tr>
	    
  <?php

	$today = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( GETDATE() AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName ORDER BY Jobs DESC");
	$i = 0;
	while ($row = mssql_fetch_array($today)){
		$i++;
?>
	    
	    <tr class="tr<?php echo ($i & 1) ?>">
	      <td><?php echo $row['nName'] ?></td>
	      <td><?php echo $row['Jobs'] ?></td>
	    </tr>
	    
  <?php

	}

	mssql_free_result($today);

	$today2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( GETDATE() AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($today2);
?>
	    
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <td>Total</td>
	      <td><?php echo $row['Total']; ?></td>
	      </tr>
	    </table>
	  </td>
	<td align="center" valign="top"><p align="center">Jobs Pending This Week</p>
	  <table cellpadding=10 class="tbljobs">
	    <colgroup style="width: 150px"></colgroup>
	    <colgroup style="width: 100px"></colgroup>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <th>Agent</th>
	      <th>Jobs</th>
	    </tr>
<?php

	mssql_free_result($today2);

	$thisweek = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE DATEPART(week, WorkDate) = DATEPART(week, GETDATE()) AND DATEPART(yyyy, WorkDate) = DATEPART(yyyy, GETDATE()) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName");
	$i = 0;
	while ($row = mssql_fetch_array($thisweek)){
		$i++;
?>
	    <tr class="tr<?php echo ($i & 1) ?>">
	      <td><?php echo $row['nName'] ?></td>
	      <td><?php echo $row['Jobs'] ?></td>
	      </tr>
<?php

	}

	mssql_free_result($thisweek);

	$thisweek2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE DATEPART(week, WorkDate) = DATEPART(week, GETDATE()) AND DATEPART(yyyy, WorkDate) = DATEPART(yyyy, GETDATE()) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($thisweek2);
?>
	    <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
	      <td>Total</td>
	      <td><?php echo $row['Total']; ?></td>
	      </tr>
	    </table>
	</td>
	<td align="center" valign="top"><p align="center">Jobs Pending Tomorrow</p>
      <table cellpadding=10 class="tbljobs">
        <colgroup style="width: 150px">
          </colgroup>
        <colgroup style="width: 100px">
          </colgroup>
        <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
          <th>Agent</th>
          <th>Jobs</th>
        </tr>
<?php

	mssql_free_result($thisweek2);

	$tomorrow = mssql_query("SELECT nName, count(*) as Jobs FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( DATEADD(dd,1,GETDATE()) AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') GROUP BY nName ORDER BY Jobs DESC");
	$i = 0;
	while ($row = mssql_fetch_array($tomorrow)){
		$i++;
?>
        <tr class="tr<?php echo ($i & 1) ?>">
          <td><?php echo $row['nName'] ?></td>
          <td><?php echo $row['Jobs'] ?></td>
        </tr>
        <?php

	}

	mssql_free_result($tomorrow);

	$tomorrow2 = mssql_query("SELECT count(*) as Total FROM tblJobs WHERE WorkDate = CAST( FLOOR( CAST( DATEADD(dd,1,GETDATE()) AS FLOAT ) ) AS DATETIME ) AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid')");
	$row = mssql_fetch_array($tomorrow2);
?>
        <tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
          <td>Total</td>
          <td><?php echo $row['Total']; ?></td>
        </tr>
      </table></td>
	</tr>
    </table>
    <div style="margin: 0 auto; text-align: center">
		<p>Detailed View</p>
      	<table align="center" cellpadding=10 class="tbljobs">
			<colgroup style="width: 160px"></colgroup>
			<colgroup style="width: 120px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 700px"></colgroup>
			<tr bgcolor='#ff6600' style="text-align: center; font-weight: bold">
				<th>Work Date/Time</th>
				<th>Agent</th>
				<th>JobID</th>
				<th>Description</th>
				<th>Value</th>
			</tr>
<?php
			mssql_free_result($tomorrow2);

			$detailedview = mssql_query("SELECT JobID, Description, nName, WorkDate, WorkStartTime FROM tblJobs WHERE WorkDate >= CURRENT_TIMESTAMP-1 AND (Status = 'Pending' OR Status = 'Completed Awaiting Invoice' OR Status = 'Invoiced' OR Status = 'Job Paid') ORDER BY WorkDate");
			$i = 0;
			while ($row = mssql_fetch_array($detailedview)){
				$i++;
				$date_day = $row['WorkDate'];
				$date = date("D jS M Y", strtotime($row['WorkDate']));
				$date_time = $row['WorkStartTime'];
				$date2 = substr($date_time, -7);
				$value = mssql_fetch_array(mssql_query("SELECT SUM(Total) AS Total FROM tblCosts WHERE (JobID = " . $row['JobID'] . ")"));
?>
				<tr class="tr<?php echo ($i & 1) ?>">
					<td><?php echo $date ?></td> <!-- echo $date . " @ " . $date2 -->
					<td><?php echo $row['nName'] ?></td>
					<td style="font-weight: bold;"><?php echo $row['JobID'] ?></td>
					<td><?php echo $row['Description'] ?></td>
					<td><?php echo "�".$value['Total'] ?></td>
				</tr>
<?php

			}

?>
	    </table>
	</div>
	</div>
</div>
<br/>

<?php

	mssql_free_result($detailedview);
	mssql_close($con);
?>
</body>
</html>