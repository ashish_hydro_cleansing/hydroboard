<?php

session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
	
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Cancel Job Form</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js"
            type="text/javascript"></script>  
        <style>
            table {
                border: 1px solid #ccc;   
                  width: 100%;
              }
            th {
                background: #f8f8f8; 
                font-weight: bold;    
                  padding: 2px;
                  color:#337ab7;
              }
        </style>
        <script>
           function cancel(){
              var job_number = $('#usr').val();
              var selectValue = $('#selectoption').val();
              var cancelJob = "cancelJob";
                $.ajax({
                        url: 'agentOperations.php',
                        type: 'GET',
                        data: { 'cmd': cancelJob, 'jobNumber': job_number, 'selectValue' : selectValue},
                        success: function(data)
                        {
                            if(data === "pass"){
                                $("#msg").show();
                                $("#failmsg").hide();
                                $("#errormsg").hide();
                            }
                            else if(data ==="fail"){
                                 $("#failmsg").show();
                                 $("#msg").hide();
                                 $("#errormsg").hide();
                            }
                            else{
                                $("#errormsg").show();
                                $("#msg").hide();
                                $("#failmsg").hide();
                            }
                            
                    }
                       
              });  
          }
        </script> 
    </head>
    <body>
        
        
        <div class="container">
            
            <h2><span class="label label-primary">Cancel a Job/Appointment</span></h2>
            <br><br> 
            
            <div class="reports">
                <form role="form" method="post" action="agentOperations.php?cmd=cancelJob">
                    <div class="form-group col-lg-6">
                        <label for="selectValue">Jobs/Appointments:</label>
                        <select class="form-control" id="selectoption" name="selectValue" required>
                            <option></option>
                            <option>Jobs</option>
                            <option>Appointments</option>
                        </select>
                    </div>
                    <div class="col-lg-10"></div>
                    <div class="form-group col-lg-6">
                        <label for="usr">Job Number:</label>
                        <input type="text" class="form-control" id="usr" name="job_number" required>
                    </div>
                    <div class="col-lg-10"></div>
                    <div class="col-lg-6">
                        <button type="button" id="enterButton" class="btn btn-primary" onclick="cancel()" >Cancel</button>
                        <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button> 
                    </div>
                    <div class="col-lg-10"></div>
                    <div class="form-group col-lg-6">
                    <div id="msg" style="display:none;color:blue;">The job number was cancelled.</div>
                    <div id="failmsg" style="display:none;color:green;">The job was already cancelled </div>
                    <div id="errormsg" style="display:none;color:red;">No such job number found. Please verify.</div>
                    </div>
                   
                </form>
            </div>
        </div> 
    </body>
</html>