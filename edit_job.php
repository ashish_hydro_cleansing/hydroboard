<?php
include("config.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  *  author : reka sambath
  */
session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Edit Job </title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/south-street/jquery-ui.css" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
        <script>
            $(function() {
            $( "#booked_date" ).datepicker({
                dateFormat: 'yy/mm/dd', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
              $( "#job_date" ).datepicker({
                dateFormat: 'yy/mm/dd', 
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
              });
          }
        </script>
    </head>
    <body>
        <div class="container">
            <h2><span class="label label-primary">Edit Job</span></h2>
            <br><br>
            <div class="alert alert-success">
                    <?php
                        if($_GET['msg'] == "success_job_details"){ 
                            echo "The Job with job number"." ".$_GET['job_number']." "."was edited";
                        }
                        else{
                            echo "Editing Job";
                        }
                     ?>
                </div>
            <form role="form" method="post" action="agentOperations.php?cmd=edit_job">
                <div class="form-group col-lg-6">
                    <label for="job_number">Enter Job number:</label>
                    <input type="text" class="form-control" id="job_number" name="job_number" value="<?php echo $_GET['job_number'] ?>" required>
                </div>
                <div class="col-lg-10"></div>
                <div class="form-group col-lg-6">
                    <?php
                    if($_GET['msg'] == "job_details"){ ?>
                    <label for="agent_name">Agent Name: </label>
                    <input type="text" class="form-control" id="agent_name" name="agent_name" value ="<?php echo $_GET['agent_name'] ?>" required>
                    <label for="job_value">Job value: </label>
                    <input type="text" class="form-control" id="job_value" name="job_value" value ="<?php echo $_GET['job_value'] ?>" required>
                    <label for="points_value">Points value: </label>
                    <input type="text" class="form-control" id="points_value" name="points_value" value ="<?php echo $_GET['points_value'] ?>" required>
                    <label for="agent_name">Company Name: </label>
                    <input type="text" class="form-control" id="company_name" name="company_name" value ="<?php echo $_GET['company_name'] ?>">
                    <label for="job_date">Job Date(dd-mm-yyyy):</label>
                    <input type="text" class="form-control" id="job_date" name="job_date"  value ="<?php echo $_GET['job_date'] ?>" required>
                    <?php }?>
                </div>
                <div class="col-lg-10"></div>
                <div class="col-lg-6">
                    <?php
                    if($_GET['msg'] == "job_details"){ 
                       ?>
                   <button type="submit" class="btn btn-primary">Save</button>
                     <input type="hidden" name="varname" value="var_value">
                    <?php }
                    else{ ?>
                        <button type="submit" class="btn btn-primary">Edit</button>
                  
                    <?php   } ?>
                    <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button>
                </div>
            </form>
        </div>
    </body>
</html>    
