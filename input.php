<?php
	session_start();

	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
	
/*	echo "Session ID: ".session_id()."</br>";
	Print_r ($_SESSION);	*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
	<title>Hydro-Board - Input Form</title>

	<link rel="stylesheet" type="text/css" href="css/allreset.css">
	<link rel="stylesheet" type="text/css" href="css/input.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<script language="JavaScript" src="calendar_db.js"></script>
	<script language="JavaScript" src="jspostcode.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">

	function checkRepName() {
		if (document.input_app_form.rep_name.value=="") {
			alert ("Please select a Rep Name for this appointment");
			document.input_app_form.rep_name.focus();
		}
		else document.input_app_form.submit();return false
	}

	</script>

<?php

	if ($_GET['msg']=="jobduplicate") {
		echo "<SCRIPT LANGUAGE=\"javascript\">";
		echo "alert (\"Failed: Duplicate Job number found - Board not updated\")";
		echo "</SCRIPT>";
	}
	
	if ($_GET['msg']=="appduplicate") {
		echo "<SCRIPT LANGUAGE=\"javascript\">";
		echo "alert (\"Failed: Duplicate Application number found - Board not updated\")";
		echo "</SCRIPT>";
	}
?>

</head>

<body onLoad="self.focus();document.input_job_form.job_number.focus()">

<?php

	include("config.php");

	$username = $_SESSION["username"];
	$del_job = $_GET['del_job'];
	$del_app = $_GET['del_app'];
	$message = "";
	$jn = $_GET['jn'];
	$time_now = date ('H:i:s');
	$date_today = date("Y-m-d");
	$day_number = date('w');
	$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);

	if (is_null($_GET['order'])) {
		$sort_order = "DESC";
	}
		else {
			$sort_order = $_GET['order'];
		}

	if (is_null($_GET['day_job_sort'])) {
		$day_job_sort = "job_number";
	}
		else {
			$day_job_sort = $_GET['day_job_sort'];
		}

	if (is_null($_GET['week_job_sort'])) {
		$week_job_sort = "job_number";
	}
		else {
			$week_job_sort = $_GET['week_job_sort'];
		}

	if (is_null($_GET['day_app_sort'])) {
		$day_app_sort = "job_number";
	}
		else {
			$day_app_sort = $_GET['day_app_sort'];
		}

	if (is_null($_GET['week_app_sort'])) {
		$week_app_sort = "job_number";
	}
		else {
			$week_app_sort = $_GET['week_app_sort'];
		}

	if ($_GET['msg'] == "jobduplicate") {
		$message = 'Failed: Job number [ '.$jn.' ] is already on the board.';
	}

	if ($_GET['msg'] == "appduplicate") {
		$message = 'Failed: Appointment number [ '.$jn.' ] is already on the board.';
	}

	if ($_GET['msg'] == "jobupdated") {
		$message = 'Successful: Job number [ '.$jn.' ] successfully added.';
	}
	
	if ($_GET['msg'] == "appupdated") {
		$message = 'Successful: Appointment number [ '.$jn.' ] successfully added.';
	}
	
	if ($_GET['msg'] == "jobcancelled") {
		$message = 'Cancelled: Job number [ '.$jn.' ] successfully cancelled.';
	}
	
	if ($_GET['msg'] == "appcancelled") {
		$message = 'Cancelled: Appointment number [ '.$jn.' ] successfully cancelled.';
	}
	
	if ($_GET['msg'] == "jobrestored") {
		$message = 'Restored: Job number [ '.$jn.' ] successfully restored.';
	}
	
	if ($_GET['msg'] == "apprestored") {
		$message = 'Restored: Appointment number [ '.$jn.' ] successfully restored.';
	}
	
	if ($_GET['msg'] == "jobdeleted") {
		$message = 'Deleted: Job number [ '.$jn.' ] successfully deleted.';
	}
	
	if ($_GET['msg'] == "appdeleted") {
		$message = 'Deleted: Appointment number [ '.$jn.' ] successfully deleted.';
	}
        
        if(!isset($POST['user'])){
             $user = null;
        }
?>

<div id="container" >

	<div id="header">
		<img src="images/hydro_logo.png" align="left">
		<img src="images/hydro_logo.png" align="right">
		<h3>Hydro-Board - Input Form</h3>
	</div>

	<table>
		<tr>
			<td>
				<form class="menu">
					<input type="button" class="btn" value="Log-Out" onClick="parent.location='index.php'"> 
                                        <input type="button" class="btn" value="Refresh" onClick="parent.location='input.php'">
				</form>
			</td>
                        <td>
                            <?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Taylor Chinnery" || $_SESSION["username"] == "Ella Plested"){ ?>
                                <input type="button" class="btn" value="Add Agent" onClick="parent.location='addAgent.php'"> 
                                <input type="button" class="btn" value="Disable Agent" onClick="parent.location='deleteUser.php'">
                                <input type="button" class="btn" value="Change Password" onClick="parent.location='changePassword.php'"> 
                            <?php } ?>
                            <?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Ella Plested" || $_SESSION["username"] == "Taylor Chinnery"){ ?>
                                <input type="button" class="btn" value="Edit Job" onclick="parent.location='edit_job.php'">
                                <?php } ?>
                                 <?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Ella Plested" || $_SESSION["username"] == "Taylor Chinnery"){ ?>
                                <input type="button" class="btn" value="Generate Reports" onclick="parent.location='generate_reports.php'">
                                <?php } ?>
                                <?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Ella Plested" || $_SESSION["username"] == "Taylor Chinnery" ){ ?>
                                <input type="button" class="btn" value="Cancel Job/Appointment" onclick="parent.location='cancelajob.php'">
                                <input type="button" class="btn" value="Calender" onclick="parent.location='calenderview.php'">
                            <?php } ?> 
							
                                <?php if($_SESSION["role_id"] == "4" || $_SESSION["role_id"] == "7" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Taylor Chinnery" || $_SESSION["username"] == "Joshua Witter" || $_SESSION["username"] == "Charles Garland" || $_SESSION["username"] == "Leanne Mok" || $_SESSION["username"] == "Jackie King" || $_SESSION["username"] == "Conor Wilson-Bracken"  || $_SESSION["username"] == "Stephen Roberts" || $_SESSION["username"] == "Ken Antwi"){ ?>
                                <input type="button" class="btn" value="Customer Care Services" onclick="parent.location='customercareservices.php'">
								<input type="button" class="btn" value="Customer Care Report" onclick="parent.location='CustomerCareReport.php'">
                            <?php } ?> 
                        </td>
                        
			<td id="message">
				<?php echo $message; ?>
			</td>
		</tr>
	</table>
    
<?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Ella Plested" || $_SESSION["username"] == "Ken Antwi" || $_SESSION["username"] == "Leanne Mok"){ ?>
	<table align="Center">
	<tr>
	<td valign="top">
	<div id="input">

		<p id="tab-10">Enter a job</p>

		<form name="input_job_form" method="post" action="<?php echo "insert.php?cmd=add_job";?>">
		<table cellpadding="4" cellspacing="1">
			<colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup></colgroup>
                        <colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup style="width: 220px"></colgroup>
			<colgroup></colgroup>

			<tr align="center" bgcolor='#FF6600' style="font-weight: bold">
				<th>Agent</th>
				<th>Job No.</th>
				<th>Job Type</th>
				<th>Jobs</th>
                                <th>Company</th>
				<th>Figures(�)</th>
				<th>Job Date</th>
				<th>Submit</th>
			</tr>
			<tr valign="top">
				<td>
					<select name="agent_name" onkeydown="if(event.keyCode==13)event.keyCode=9">
<?php

	$result = mysql_query("SELECT agent_name FROM agents WHERE enabled='1' ORDER BY agent_name ASC");
	while ($row = mysql_fetch_array($result)) {
		echo '<option value="'.$row['agent_name'].'"';
		if ($row['agent_name'] == $username) {
			echo ' SELECTED ';
		}
		echo '>'.$row['agent_name'].'</option>'.$row['agent_name'];
	}
?>

					</select>
				</td>
				<td>
					<input type="text" name="job_number" style="width: 120px" onKeyDown="if(event.keyCode==13)event.keyCode=9" maxlength=15 onKeyUp="if(job_number.value.length==15)document.input_job_form.job_type.focus()">
				</td>
				<td>
					<select name="job_type" style="width: 480px;" onkeydown="if(event.keyCode==13)event.keyCode=9">
					<option value="">
<?php

	$job_type = mysql_query("SELECT job_type FROM job_types ORDER BY job_type ASC");
	while($row = mysql_fetch_array($job_type)) {
		echo '<option value="'.$row['job_type'].'">'.$row['job_type'].'</option>';
	}
?>

					</select>
				</td>
				<td>
					<input type="text" name="job_value" style="width: 80px;" onKeyDown="if(event.keyCode==13)event.keyCode=9">
				</td>
                                <td>
					<input type="text" name="company_name" style="width: 80px;" onKeyDown="if(event.keyCode==13)event.keyCode=9">
				</td>
				<td>
					<input type="text" name="points_value" style="width: 80px;">
				</td>
				<td>
				<input type="text" name="job_date" style="width: 160px;">
					<script language="JavaScript">
					new tcal ({'formname': 'input_job_form','controlname': 'job_date'});
					</script>
				</td>
				<td>
					<input type="image" name="button1" src="images/hydro_logo.png" height="33px" width="100px" BORDER=0 onClick="javascript:document.input_job_form.submit();return false">
				</td>
			</tr>
		</table>
		</form>

		<p id="tab-10">Enter an appointment</p>

		<form name="input_app_form" method="post" action="<?php echo "insert.php?cmd=add_app";?>">
		<table cellpadding="4" cellspacing="1">
			<colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup></colgroup>
                        <colgroup></colgroup>
			<colgroup></colgroup>
			<colgroup style="width: 220px"></colgroup>
			<colgroup></colgroup>

			<tr align="center" bgcolor='#0066cc' style="font-weight: bold">
				<th>Agent</th>
				<th>Job No.</th>
				<th>Rep Name</th>
                                <th>Company</th>
				<th>Postcode</th>
				<th>Time</th>
				<th>Date</th>
				<th>Submit</th>
			</tr>
			<tr valign="top">
				<td>
					<select name="agent_name" onkeydown="if(event.keyCode==13)event.keyCode=9">

<?php

	$result = mysql_query("SELECT agent_name FROM agents WHERE enabled='1' ORDER BY agent_name ASC");
	while ($row = mysql_fetch_array($result)) {
		echo '<option value="'.$row['agent_name'].'"';
		if ($row['agent_name'] == $username) {
			echo ' SELECTED ';
		}
		echo '>'.$row['agent_name'].'</option>'.$row['agent_name'];
	}
?>

					</select>
				</td>
				<td>
					<input type="text" name="job_number" style="width: 120px;" onKeyDown="if(event.keyCode==13)event.keyCode=9" maxlength=15 onKeyUp="if(job_number.value.length==15)document.input_app_form.rep_name.focus()">
				</td>
				<td>
					<select name="rep_name" style="width: 264px;" onkeydown="if(event.keyCode==13)event.keyCode=9">
					<option value="">

<?php

	$rep_name = mysql_query("SELECT rep_name FROM reps WHERE enabled='1' ORDER BY rep_name ASC");
	while($row = mysql_fetch_array($rep_name)) {
		echo '<option value="'.$row['rep_name'].'">'.$row['rep_name'].'</option>';
	}
?>

					</select>
				</td>
                                <td>
					<input type="text" name="company_name" style="width: 160px;" onKeyDown="if(event.keyCode==13)event.keyCode=9">
				</td>
				<td>
					<input type="text" name="app_postcode" style="width: 200px;" onKeyDown="if(event.keyCode==13)event.keyCode=9">
				</td>
                                
				<td>
					<input type="text" name="app_time" style="width: 176px;" onKeyDown="if(event.keyCode==13)event.keyCode=9">
				</td>
				<td>
					<input type="text" name="app_date" style="width: 160px;">
					<script language="JavaScript">
						new tcal ({'formname': 'input_app_form','controlname': 'app_date'});
					</script>
				</td>
				<td>
					<input type="image" name="button1" src="images/hydro_logo.png" height="33px" width="100px" BORDER=0 onClick="checkRepName();return false">
				</td>
			</tr>
		</table>
		</form>
	</div>
   <?php } ?>    
	<?php if($_SESSION["role_id"] == "4" || $_SESSION["username"] == "Shane Lindsay" || $_SESSION["username"] == "Ella Plested" || $_SESSION["username"] == "Taylor Chinnery" || $_SESSION["username"] == "Leanne Mok" || $_SESSION["username"] == "Connor Barnes"){ ?>
	<div id="reports">
	<table align="center">
	<tr>
	<td valign="top">
	
		<p id="tab-10">Today's Jobs</p>

		<table class="tbljobs">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 200px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
                        <colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
<!--			<colgroup style="width: 55px"></colgroup> -->

			<tr>
				<th><?php echo '<a href="?day_job_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?day_job_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?day_job_sort=job_type&order=ASC">' ?>Job Type</a></th>
				<th><?php echo '<a href="?day_job_sort=job_value&order=ASC">' ?>Jobs</a></th>
                                <th><?php echo '<a href="?day_job_sort=company_name&order=ASC">' ?>Company</a></th>
				<th><?php echo '<a href="?day_job_sort=points_value&order=ASC">' ?>Figures(�)</a></th>
				<th><?php echo '<a href="?day_job_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th align="center">Cancel</th>
<!--				<th align="center">Delete</th> -->
			</tr>

<?php

	$result = mysql_query("SELECT * FROM jobs WHERE booked_date = '$date_today' && cancelled = '0' ORDER BY `jobs`.`$day_job_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
		$int_job_value = intval($row['job_value']);
		$int_points_value = intval($row['points_value']);
?>
			<tr <?php if ($odd = $i%2 ) { echo "bgcolor='#f8ee3a'"; } else { echo "bgcolor='#ffcd35'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['job_type'] ?></td>
				<td><?php if ($int_job_value == $row['job_value']) {echo (int) $int_job_value;} else {echo $row['job_value'];} ?></td>
				<td><?php echo $row['company_name'] ?></td>
                                <td><?php if ($int_points_value == $row['points_value']) {echo (int) $int_points_value;} else {echo $row['points_value'];} ?></td>
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=cancel_job&can_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to cancel this job?\')) submit();"><img src="images/cancel.png"  border=0></a>';?></td>
<!--				<td align="center"><?php echo '<a href="insert.php?cmd=delete_job&del_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  border=0></a>';?></td> -->
			</tr>

<?php

	$i++;
	}
?>

		</table>
		
	</td>
	<td valign="top">

		<p id="tab-10">Today's Appointments</p>

		<table class="tblapps">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 120px"></colgroup>
                        <colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
<!--			<colgroup style="width: 55px"></colgroup> -->

			<tr>
				<th><?php echo '<a href="?day_app_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?day_app_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?day_app_sort=rep_name&order=ASC">' ?>Rep</a></th>
                                <th><?php echo '<a href="?day_app_sort=company_name&order=ASC">' ?>Company</a></th>
				<th><?php echo '<a href="?day_app_sort=app_postcode&order=ASC">' ?>Postcode</a></th>
				<th><?php echo '<a href="?day_app_sort=app_time&order=ASC">' ?>App Time</a></th>
				<th><?php echo '<a href="?day_app_sort=app_date&order=ASC">' ?>App Date</a></th>
				<th><?php echo '<a href="?day_app_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th align="center">Cancel</th>
<!--				<th align="center">Delete</th> -->
			</tr>

<?php

	$result = mysql_query("SELECT * FROM apps WHERE booked_date = '$date_today' && cancelled = '0' ORDER BY `apps`.`$day_app_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
?>

			<tr <?php if ($odd = $i%2 ) { echo "bgcolor='#0099cc'"; } else { echo "bgcolor='#00ccff'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['rep_name'] ?></td>
                                <td><?php echo $row['company_name'] ?></td>
				<td><?php echo $row['app_postcode'] ?></td>
				<td><?php echo date("H.i",strtotime($row['app_time'])) ?></td>
				<td><?php echo date("j F", strtotime($row['app_date'])) ?></td>
				<td><?php echo date("H.i",strtotime($row['booked_time'])) ?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=cancel_app&can_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to cancel this appointment?\')) submit();"><img src="images/cancel.png"  BORDER=0></a>';?></td>
<!--				<td align="center"><?php echo '<a href="insert.php?cmd=delete_app&del_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  BORDER=0></a>';?></td> -->
			</tr>

<?php

	$i++;}
?>

		</table>

	</td>
	</tr>
	<tr>
	<td valign="top">

		<p id="tab-10">This Week's Jobs</p>

		<table class="tbljobs">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 200px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
                        <colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
<!--		<colgroup style="width: 55px"></colgroup> -->

			<tr>
				<th><?php echo '<a href="?week_job_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?week_job_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?week_job_sort=job_type&order=ASC">' ?>Job Type</a></th>
				<th><?php echo '<a href="?week_job_sort=job_value&order=ASC">' ?>Jobs</a></th>
                                <th><?php echo '<a href="?week_job_sort=company_name&order=ASC">' ?>Company</a></th>
				<th><?php echo '<a href="?week_job_sort=points_value&order=ASC">' ?>Figures(�)</a></th>
				<th><?php echo '<a href="?week_job_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th>Cancel</th>
<!--			<td align="center">Delete Job</td> -->
			</tr>

<?php

	$result = mysql_query("SELECT * FROM jobs WHERE booked_date >= '$week_begin' && cancelled = '0' ORDER BY `jobs`.`$week_job_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
		$int_job_value = intval($row['job_value']);
		$int_points_value = intval($row['points_value']);
?>

			<tr	<?php if ($odd = $i%2 ) { echo "bgcolor='#f8ee3a'"; } else { echo "bgcolor='#ffcd35'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['job_type'] ?></td>
				<td><?php if ($int_job_value == $row['job_value']) {echo (int) $int_job_value;} else {echo $row['job_value'];} ?></td>
				<td><?php echo $row['company_name'] ?></td>
                                <td><?php if ($int_points_value == $row['points_value']) {echo (int) $int_points_value;} else {echo $row['points_value'];} ?></td>
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=cancel_job&can_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to cancel this job?\')) submit();"><img src="images/cancel.png"  BORDER=0></a>';?></td>
<!--			<td align="center"><?php echo '<a href="insert.php?cmd=delete_job&del_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  BORDER=0></a>';?></td> -->
			</tr>

<?php

		$i++;
	}
?>

		</table>

	</td>
	<td valign="top">

		<p id="tab-10">This Week's Appointments</p>

		<table class="tblapps">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 120px"></colgroup>
                        <colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
<!--		<colgroup style="width: 55px"></colgroup> -->

			<tr>
				<th><?php echo '<a href="?week_app_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?week_app_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?week_app_sort=rep_name&order=ASC">' ?>Rep</a></th>
                                <th><?php echo '<a href="?week_app_sort=company_name&order=ASC">' ?>Company</a></th>
				<th><?php echo '<a href="?week_app_sort=app_postcode&order=ASC">' ?>Postcode</a></th>
				<th><?php echo '<a href="?week_app_sort=app_time&order=ASC">' ?>App Time</a></th>
				<th><?php echo '<a href="?week_app_sort=app_date&order=ASC">' ?>App Date</a></th>
				<th><?php echo '<a href="?week_app_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th>Cancel</th>
<!--			<th>Delete App</th> -->
			</tr>

<?php

	$result = mysql_query("SELECT * FROM apps WHERE booked_date >= '$week_begin' && cancelled = '0' ORDER BY `apps`.`$week_app_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
?>

			<tr <?php if ($odd = $i%2 ) { echo "bgcolor='#0099cc'"; } else { echo "bgcolor='#00ccff'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['rep_name'] ?></td>
                                <td><?php echo $row['company_name'] ?></td>
				<td><?php echo $row['app_postcode'] ?></td>
				<td><?php echo date("H.i",strtotime($row['app_time'])) ?></td>
				<td><?php echo date("j F", strtotime($row['app_date'])) ?></td>
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=cancel_app&can_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to cancel this appointment?\')) submit();"><img src="images/cancel.png"  BORDER=0></a>';?></td>
<!--			<td align="center"><?php echo '<a href="insert.php?cmd=delete_app&del_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  BORDER=0></a>';?></td> -->
			</tr>

<?php

		$i++;
	}
?>

		</table>

	</td>
	</tr>
	<tr>
	<td valign="top">

		<p id="tab-10">This Week's Cancelled Jobs</p>

		<table class="tblcancelled">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 200px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
                        <colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
			<colgroup style="width: 55px"></colgroup>

			<tr>
				<th><?php echo '<a href="?week_job_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?week_job_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?week_job_sort=job_type&order=ASC">' ?>Job Type</a></th>
				<th><?php echo '<a href="?week_job_sort=job_value&order=ASC">' ?>Jobs</a></th>
                                <th><?php echo '<a href="?week_job_sort=company_name&order=ASC">' ?>Company</a></th>
				<th><?php echo '<a href="?week_job_sort=points_value&order=ASC">' ?>Figures(�)</a></th>
				<th><?php echo '<a href="?week_job_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th>Restore</th>
				<th>Delete Job</th>
			</tr>

<?php

	$result = mysql_query("SELECT * FROM jobs WHERE booked_date >= '$week_begin' && cancelled = '1' && removed = '0' ORDER BY `jobs`.`$week_job_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
		$int_job_value = intval($row['job_value']);
		$int_points_value = intval($row['points_value']);
?>

			<tr	<?php if ($odd = $i%2 ) { echo "bgcolor='#dd6666'"; } else { echo "bgcolor='#dd3333'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['job_type'] ?></td>
				<td><?php if ($int_job_value == $row['job_value']) {echo (int) $int_job_value;} else {echo $row['job_value'];} ?></td>
				<td><?php echo $row['company_name'] ?></td>
                                <td><?php if ($int_points_value == $row['points_value']) {echo (int) $int_points_value;} else {echo $row['points_value'];} ?></td>
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=restore_job&res_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to restore this job?\')) submit();"><img src="images/restore.png"  BORDER=0></a>';?></td>
				<td align="center"><?php echo '<a href="insert.php?cmd=delete_job&del_job='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  BORDER=0></a>';?></td>
			</tr>

<?php

		$i++;
	}
?>

		</table>

	</td>
	<td valign="top">

		<p id="tab-10">This Week's Cancelled Appointments</p>

		<table class="tblcancelled">
			<colgroup style="width: 100px"></colgroup>
			<colgroup style="width: 60px"></colgroup>
			<colgroup style="width: 120px"></colgroup>
                        <colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 80px"></colgroup>
			<colgroup style="width: 70px"></colgroup>
			<colgroup style="width: 55px"></colgroup>
			<colgroup style="width: 55px"></colgroup>

			<tr>
				<th><?php echo '<a href="?week_app_sort=agent_name&order=ASC">' ?>Agent</a></th>
				<th><?php echo '<a href="?week_app_sort=job_number&order=DESC">' ?>Job No.</a></th>
				<th><?php echo '<a href="?week_app_sort=rep_name&order=ASC">' ?>Rep</a></th>
                                <th><?php echo '<a href="?week_app_sort=rep_name&order=ASC">' ?>Company</a></th>
                                <th><?php echo '<a href="?week_app_sort=app_postcode&order=ASC">' ?>Postcode</a></th>
				<th><?php echo '<a href="?week_app_sort=app_time&order=ASC">' ?>App Time</a></th>
				<th><?php echo '<a href="?week_app_sort=app_date&order=ASC">' ?>App Date</a></th>
				<th><?php echo '<a href="?week_app_sort=booked_date&order=ASC">' ?>Booked</a></th>
				<th>Restore</th>
				<th>Delete App</th>
			</tr>

<?php

	$result = mysql_query("SELECT * FROM apps WHERE booked_date >= '$week_begin' && cancelled = '1' && removed = '0' ORDER BY `apps`.`$week_app_sort` $sort_order");
	$num = mysql_numrows($result);
	$i=0;

	while ($i < $num) { $row = mysql_fetch_array($result);
?>

			<tr <?php if ($odd = $i%2 ) { echo "bgcolor='#dd6666'"; } else { echo "bgcolor='#dd3333'"; }?><?php if ($row['agent_name'] == $user) { echo ' style="font-style: italic; font-weight: bold;"';}?>>
				<td><?php echo $row['agent_name'] ?></td>
				<td><?php echo $row['job_number'] ?></td>
				<td><?php echo $row['rep_name'] ?></td>
                                <td><?php echo $row['company_name'] ?></td>
				<td><?php echo $row['app_postcode'] ?></td>
				<td><?php echo date("H.i",strtotime($row['app_time'])) ?></td>
				<td><?php echo date("j F", strtotime($row['app_date'])) ?></td>
				<td><?php echo date('l', strtotime($row['booked_date'])) ?></td>
				<td><?php echo '<a href="insert.php?cmd=restore_app&res_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to restore this appointment?\')) submit();"><img src="images/restore.png"  BORDER=0></a>';?></td>
				<td><?php echo '<a href="insert.php?cmd=delete_app&del_app='.$row['job_number'].'" onclick="if (confirm(\'Are you SURE you want to delete this record?\')) submit();"><img src="images/delete.png"  BORDER=0></a>';?></td>
			</tr>

<?php

		$i++;
	}
	
	mysql_close($con);
?>

		</table>
	</td>
	</tr>
	</table>
	</div>
	<?php } ?>
	</td>
	</tr>
	</table>
</div>
<br/>
</body>
</html>