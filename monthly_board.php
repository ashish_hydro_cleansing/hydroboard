<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<meta http-equiv="refresh" content="30"/>
	<script src="js/core.js"></script>
        <script>
			function loadData(){
				$.ajax({
					url:"model/MonthlyBoardBackEnd.php",
					type:"POST",
					dataType:"json",
					success: function(rs){
						console.log("poll");
						renderData(rs);
					}
				});
			}
			function loader(){
				setTable();
				loadData();
			}
            window.addEventListener("load",loader);
            window.addEventListener("resize",setTable);
        </script>
        <title>Monthly Hydro-Board Wallboard</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <link href="css/board.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body id="roadsweeper">
    <svg style="display:none; position: absolute;" id="circle">
        <clipPath id="clip">
            <circle cx="0" cy="487.5" r="1290" fill="red"/>
        </clipPath>
    </svg>
        <div id="window"></div>
        <div id="screen">
                <div id="tableTop">
                    <div id="logo"><img src="logo.png"></div>
                    <div class="row" id="column-headings">
					<div style="margin-left:-150px;">
					<div class="column">MEGA-DONS</div>
						<div class="column">STREET SWEEP</div>
                        <div class="column">INC BULKS</div>						
                        <div class="column">W W MAFIA</div>
                        <div class="column">TOTAL(REVENUE)</div>	
					</div>                        
                    </div>
                    <div id="table">
                        <div class="row" id="day">
                           <!-- <div class="apps day column"></div> -->
                            <div class="points day column columnextra"></div>
							<div class="pointsWWM day column columnextra"></div>
							<div class="pointsIB day column columnextra"></div>
							<div class="pointsSS day column columnextra"></div>
							<div class="pointsMD day column columnextra"></div>
							<!--<div class="jobs day column"></div> -->
                        </div>						
						<div class="row" id="nextday">
                            <!--<div class="apps nextday column"></div>-->
                            <div class="points nextday column columnextra"></div>
							<div class="pointsWWM nextday column columnextra"></div>
							<div class="pointsIB nextday column columnextra"></div>
							<div class="pointsSS nextday column columnextra"></div>
							<div class="pointsMD nextday column columnextra"></div>
                           <!-- <div class="jobs nextday column"></div> -->
                        </div>
						 <div class="row" id="secondday">
                            <!--<div class="apps secondday column"></div>-->
                            <div class="points secondday column columnextra"></div>
							<div class="pointsWWM secondday column columnextra"></div>
							<div class="pointsIB secondday column columnextra"></div>
							<div class="pointsSS secondday column columnextra"></div>
							<div class="pointsMD secondday column columnextra"></div>
                            <!--<div class="jobs secondday column"></div>-->
                        </div>
						<div class="row" id="thirdday">
                           <!-- <div class="apps thirdday column"></div>-->
                            <div class="points thirdday column columnextra"></div>
							<div class="pointsWWM thirdday column columnextra"></div>
							<div class="pointsIB thirdday column columnextra"></div>
							<div class="pointsSS thirdday column columnextra"></div>
							<div class="pointsMD thirdday column columnextra"></div>
                            <!--<div class="jobs thirdday column"></div>-->
                        </div>
						 <div class="row" id="fourthday">
                           <!-- <div class="apps thirdday column"></div> -->
                            <div class="points fourthday column columnextra"></div>
							<div class="pointsWWM fourthday column columnextra"></div>
							<div class="pointsIB fourthday column columnextra"></div>
							<div class="pointsSS fourthday column columnextra"></div>
							<div class="pointsMD fourthday column columnextra"></div>
                            <!--<div class="jobs thirdday column"></div> -->
                        </div> 
                       
                        <div class="sideheading">
                            <div class="day column heading">
                                <div class="caption">
                                    <!--<span>today</span>--><span class="yellText date"><?php echo date("d.m.y");?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date("l", mktime())); ?></span>
                                </div>
                            </div>
							<div class="day column heading">
                                <div class="caption">
                                    <span class="yellText date"><?php echo date('d.m.y', strtotime($Date. ' + 1 days')); ?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date('l', strtotime($Date. ' + 1 days'))); ?></span>
                                </div>
                            </div>
							<div class="day column heading">
                                <div class="caption">
                                    <span class="yellText date"><?php echo date('d.m.y', strtotime($Date. ' + 2 days'));?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date('l', strtotime($Date. ' + 2 days')));?></span>
                                </div>
                            </div>
							<div class="day column heading">
                                <div class="caption">
                                    <span class="yellText date"><?php echo date('d.m.y', strtotime($Date. ' + 3 days'));?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date('l', strtotime($Date. ' + 3 days'))); ?></span>
                                </div>
                            </div>
							<div class="day column heading">
                                <div class="caption">
                                    <span class="yellText date"><?php echo date('d.m.y', strtotime($Date. ' + 4 days'));?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date('l', strtotime($Date. ' + 4 days'))); ?></span>
                                </div>
                            </div>
                           
                        </div>
                    </div>
					
                    <!--<div id="boardBanner">
                        <div id="boardHeader"><h1>Monthly Board</h1></div>                        
                    </div> -->
                </div>
            </div>
    </body>
</html>