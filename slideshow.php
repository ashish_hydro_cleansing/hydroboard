<style>

#slideshow {
  width: 500px;
  height: 300px;
  overflow: hidden;
  position: relative; /* not required, slideshow will set this for you */ 
}
 
#slideshow > * {
  position: absolute; /* required for most transitions */ 
  top: 0;             /* ditto */ 
  left: 0;            /* ditto */ 
  width: 100%;        /* usually required */ 
  height: 100%;       /* same */
}

</style>

<div id=slideshow> 
  <div>Slide 1</div> 
  <img src=image.jpg> 
  <div>Slide 3</div> 
</div>

<script language="javascript">

var slideshow = new SlideShow('slideshow', { 
  transition: 'fadeThroughBackground', 
  delay: 5000, 
  duration: 400, 
  autoplay: true 
});

</script>