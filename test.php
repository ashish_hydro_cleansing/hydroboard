<?php
$dbHost = "hclsql";
$dbUser = "Hydrosql";
$dbPass = "Cleaning101";
$dbName = "HydroSequel"; 

//connection to the database
$con = mssql_connect($dbHost, $dbUser, $dbPass) or die ('MsSQL connect failed. ' . mssql_error());

//select a database to work with
mssql_select_db($dbName) or die ('Cannot select database.' . mssql_error()); 

//declare the SQL statement that will query the database
$query = "SELECT * ";
$query .= "FROM tblJobs ";
$query .= "WHERE({ fn WEEK(WorkDate) } = 21) AND (YEAR(WorkDate) = 2012) ";
$query .= "AND Status = 'Pending'";
$query .= "ORDER BY WorkDate";

//execute the SQL query and return records
$result = mssql_query($query);

$numRows = mssql_num_rows($result); 
echo "<h1>" . $numRows . " Row" . ($numRows == 1 ? "" : "s") . " Returned </h1>"; 

//display the results 
while($row = mssql_fetch_array($result))
{
  echo "<li>" . $row["JobID"] . " " . $row["nName"] . " " . $row["WorkDate"] . "</li>";
}
//close the connection
mssql_close($con);
?>