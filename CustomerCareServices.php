<?php
session_start();
header('Cache-control: private');
	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
	
	include("config.php");
	
			$db = new mysqli('localhost','root','hydro123','hydro');//set your database handler
  $query = "SELECT Clas1_ID, Clas1Name FROM csclas_1";
  $result = $db->query($query);

  while($row = $result->fetch_assoc()){
    $categories[] = array("id" => $row['Clas1_ID'], "val" => $row['Clas1Name']);
  }

  $query = "SELECT Clas2_ID, Clas2Name, Clas1_ID FROM csclas_2";
  $result = $db->query($query);

  while($row = $result->fetch_assoc()){
    $subcats[$row['Clas1_ID']][] = array("id" => $row['Clas2_ID'], "val" => $row['Clas2Name']);
  }
  
  $query = "SELECT Clas3_ID, Clas3Name, Clas2_ID FROM csclas_3";
  $result = $db->query($query);

  while($row = $result->fetch_assoc()){
    $supersubcats[$row['Clas2_ID']][] = array("id" => $row['Clas3_ID'], "val" => $row['Clas3Name']);
  }
	
  $query = "SELECT DepartmentID, DepartmentName FROM Department";
  $result = $db->query($query);

  while($row = $result->fetch_assoc()){
    $supersubDepart[] = array("id" => $row['DepartmentID'], "val" => $row['DepartmentName']);
  }

  
  $jsonCats = json_encode($categories);
  $jsonSubCats = json_encode($subcats);
  $jsonSuperSubCats = json_encode($supersubcats);
   $jsonSuperSubDepart = json_encode($supersubDepart);
  //echo $jsonSubCats;
  //echo $jsonSuperSubCats;
				
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Customer Services</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body  onload='loadCategories()'>
        
        <div class="container">
            <h2><span class="label label-primary">Customer Services</span></h2>
            <br><br>
				
			<script type='text/javascript'>
      <?php
        echo "var categories = $jsonCats; \n";
        echo "var subcats = $jsonSubCats; \n";
		echo "var supersubcats = $jsonSuperSubCats; \n";
		echo "var supersubDepart = $jsonSuperSubDepart; \n";
      ?>
      function loadCategories(){
        var select = document.getElementById("categoriesSelect");
        var subcatSelect = document.getElementById("subcatsSelect");	
        select.onchange = updateSubCats;
		subcatSelect.onchange = updateSuperSubCats;
		select.options[0] = new Option("---Please Select---",0);    
        for(var i = 0; i < categories.length; i++){
          select.options[i+1] = new Option(categories[i].val,categories[i].id);          
        }
      }
      function updateSubCats(){
        var catSelect = this;
        var catid = this.value;
        var subcatSelect = document.getElementById("subcatsSelect");
		var cat2Div = document.getElementById("cat2");	
		cat2Div.style.display = 'block';
		var cat3Div = document.getElementById("cat3");	
		cat3Div.style.display = 'none';		
		var cat4Div = document.getElementById("cat4");	
		cat4Div.style.display = 'none';
        subcatSelect.options.length = 0; //delete all options if any present
		subcatSelect.options[0] = new Option("---Please Select---",0);  
        for(var i = 0; i < subcats[catid].length; i++){
          subcatSelect.options[i+1] = new Option(subcats[catid][i].val,subcats[catid][i].id);
        }
      }	  
      function updateSuperSubCats(){
        var catSelect = this;
        var catid = this.value;
		var cat3Div = document.getElementById("cat3");	
		cat3Div.style.display = 'none';
		
		var cat4Div = document.getElementById("cat4");	
		cat4Div.style.display = 'block';
		var subDepartSelect = document.getElementById("subDepartSelect");	
        subDepartSelect.options.length = 0; //delete all options if any present
		subDepartSelect.options[0] = new Option("---Please Select---",0); 
		for(var i = 0; i < supersubDepart.length; i++){
          subDepartSelect.options[i+1] = new Option(supersubDepart[i].val,supersubDepart[i].id);          
        }
		
	    if(supersubcats[catid].length > 0){		
        var subcatSelect = document.getElementById("SupersubcatsSelect");	
        subcatSelect.options.length = 0; //delete all options if any present
		cat3Div.style.display = 'block';
		subcatSelect.options[0] = new Option("---Please Select---",0); 
		
		for(var i = 0; i < supersubcats[catid].length; i++){	
			subcatSelect.options[i+1] = new Option(supersubcats[catid][i].val,supersubcats[catid][i].id);
        }
		}
		else{		
		cat3Div.style.display = 'none';
		} 
		
      }
    </script>
	
		
			
            <div class="alert alert-success">
                <?php
            if($_GET['msg'] == "CallSaved"){ ?>
                <strong>Success!</strong> <?php 
                echo "Call saved";
            }
            else{
             echo "Add new call details here";   
            }
            ?>
            </div>
			
            <form role="form" method="post" action="agentOperations.php?cmd=Customer_services">
              <div class="form-group col-lg-6" id="CallSuccess">
                <label for="CallComplete">Call Completion:</label>
                <select class="form-control" id="CallComplete" name="CallSuccess" required>   
				<option value='Call Success'>Call Complete</option>  
				<option value='Call Hung up'>Call Hung up</option>  
				<option value='Call Disconnected'>Call disconnected</option>
                </select>
              </div>
			  <div class="col-lg-10"></div>
			  
              <div class="form-group col-lg-6">
                <label for="categoriesSelect">Catogery 1:</label>
                <select class="form-control" id="categoriesSelect" name="sel1" required>
				<option value='0'>--Please Select--</option>
                </select>
              </div>
               <div class="col-lg-10"></div>
			   
			  <div class="form-group col-lg-6" id="cat2" Style="Display:none">
                <label for="subcatsSelect">Catogery 2:</label>
                <select class="form-control" id="subcatsSelect" name="sel2" required >  
				<option value='0'>--Please Select--</option>
                </select>
              </div>
               <div class="col-lg-10"></div>
			   
			   <div class="form-group col-lg-6" id="cat3" Style="Display:none">
                <label for="SupersubcatsSelect">Catogery 3:</label>
                <select class="form-control" id="SupersubcatsSelect" name="sel3" required>   
				<option value='0'>--Please Select--</option>
                </select>
              </div>
			  <div class="col-lg-10"></div>			  
			   
			   <div class="form-group col-lg-6" id="cat4" Style="Display:none">
                <label for="subDepartSelect">Department:</label>
                <select class="form-control" id="subDepartSelect" name="sel4" required>   
				<option value='0'>--Please Select--</option>
                </select>
              </div>
			  <div class="col-lg-10"></div>
			  
              <div class="col-lg-6">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button>
              </div>
            </form>
        </div>
    </body>
</html>