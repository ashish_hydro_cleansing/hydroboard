<?php
session_start();
header('Cache-control: private');
	if (($_SESSION['loggedin'] != true)) {
		echo "<p>Please login before playing.</p>\n";
/*		echo "Session ID: ".session_id()."</br>";
		Print_r ($_SESSION);	*/
		exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta charset="utf-8">
        <title>Hydro-Board - Add Agent Form</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        
        <div class="container">
            <h2><span class="label label-primary">Add Agent</span></h2>
            <br><br>
            <div class="alert alert-success">
                <?php
            if($_GET['msg'] == "agentAdded"){ ?>
                <strong>Success!</strong> <?php 
                echo "Agent name"." ".$_GET['agent_name']." "."was added";
            }
            else{
             echo "Please enter Agent details here.";   
            }
            ?>
            </div>
            <form role="form" method="post" action="agentOperations.php?cmd=add_agent">
              <div class="form-group col-lg-6">
                <label for="usr">Agent Name:</label>
                <input type="text" class="form-control" id="usr" name="agent_name" required>
              </div>
              <div class="col-lg-10"></div>
              <div class="form-group col-lg-6">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" name="password" required>
              </div>
              <div class="col-lg-10"></div>
              <div class="form-group col-lg-6">
                <label for="pwd2">Re-Enter Password:</label>
                <input type="password" class="form-control" id="pwd2" name="password2" required>
              </div>
               <div class="col-lg-10"></div> 
              <div class="form-group col-lg-6">
                <label for="sel1">Select list:</label>
                <select class="form-control" id="sel1" name="role_id" required>
                    <option>--Select the board --</option>
                    <option>Account Managers</option>
                    <option>Manager</option>
                    <option>Road Sweeper Executives</option>
                    <option>Tele sales</option>
                    <option>Call Centre - 2</option>
					<option>Customer Care Services</option>
                </select>
              </div>
                <div class="col-lg-10"></div>
              <div class="form-group col-lg-6">
                <label for="sel2">Select list:</label>
                <select class="form-control" id="sel2" name="group_id" required>
                    <option>--Select the place --</option>
                    <option>WET WASTE MAFIA</option>
                    <option>INCREDIBLE BULK</option>
                    <option>STREET SWEEPERS</option>
					<option>MEGA-DONS</option>
					<option>None</option>
                </select>
              </div>
               <div class="col-lg-10"></div>
              <div class="col-lg-6">
              <button type="submit" class="btn btn-primary">Add</button>
              <button type="button" class="btn btn-primary" onClick="parent.location='input.php'">Home</button>
              </div>
            </form>
        </div>
    </body>
</html>