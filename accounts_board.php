<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/core.js"></script>
        <script>
			function loadData(){
				$.ajax({
					url:"model/AccountManagersBoardBackEnd.php",
					type:"POST",
					dataType:"json",
					success: function(rs){
						console.log("poll");
						renderData(rs);
					}
				});
			}
			function loader(){
				setTable();
				loadData();
			}
            window.addEventListener("load",loader);
            window.addEventListener("resize",setTable);
        </script>
        <title>Accounts Hydro-Board Wallboard</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <link href="css/board.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body id="accounts">
    <svg style="display:none; position: absolute;" id="circle">
        <clipPath id="clip">
            <circle cx="0" cy="487.5" r="1290" fill="red"/>
        </clipPath>
    </svg>
        <div id="window"></div>
        <div id="screen">
                <div id="tableTop">
                    <div id="logo"><img src="logo.png"></div>
                    <div class="row" id="column-headings">
                        <div class="jobHeading column">JOBS</div>
                        <div class="pointHeading column">POINTS</div>
                        <div class="appsHeading column">APPOINTMENTS</div>
                        <div class="appsHeading column heading"></div>
                    </div>
                    <div id="table">
                        <div class="row" id="day">
                            <div class="apps day column"></div>
                            <div class="points day column"></div>
                            <div class="jobs day column"></div>
                        </div>
                        <div class="row" id="week">
                            <div class="apps week column"></div>
                            <div class="points week column"></div>
                            <div class="jobs week column"></div>
                        </div>
                        <div class="sideheading">
                            <div class="day column heading">
                                <div class="caption">
                                    <span>today</span><span class="yellText date"><?php echo date("d.m.y");?></span>
                                    <br>
                                    <span class="bold ltspc"><?php echo strtoupper(date("l", mktime())); ?></span>
                                </div>
                            </div>
                            <div class="day column heading">
                                <div class="caption">
                                    <span>this</span>
                                    <br>
                                    <span class="bold">WEEK</span><span class="yellText"><?php echo date("W");?></span>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                    <div id="boardBanner">
                        <div id="boardHeader"><h1>Accounts Target Board</h1></div>
                        
                    </div>
                </div>
            </div>
    </body>
</html>