<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
         <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <title></title>
        
	<meta http-equiv="refresh" content="100" >
	<style>
		.panel{
		border:0px;
		}
		.panel-red > .panel-heading-custom {
		background: #E24E42; color: #fff; }

		.panel-yellow > .panel-heading-custom {
		background: #E9B000; color: #fff; }

		.panel-green > .panel-heading-custom {
		background: #008F95; color: #fff; }
		
		.panel-black > .panel-heading-custom {
		background: #666666; color: #fff; }

		.panel-orange > .panel-heading-custom {
		background: #ecf0f1; color: #FB6B02; }

		.panel-body.panel-bodyOne {
		background:#C93756;
		}
		.panel-body.panel-bodyTwo {
		background:#34495e;
		}
		.panel-body.panel-bodyThree {
		background:#26A65B;
		}
		.col-xs-5ths,
		.col-sm-5ths,
		.col-md-5ths,
		.col-lg-5ths {
			position: relative;
			min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
		}

		.col-xs-5ths {
			width: 20%;
			float: left;
		}

		@media (min-width: 768px) {
			.col-sm-5ths {
				width: 20%;
				float: left;
			}
		}

		@media (min-width: 992px) {
			.col-md-5ths {
				width: 20%;
				float: left;
			}
		}

		@media (min-width: 1200px) {
			.col-lg-5ths {
				width: 20%;
				float: left;
			}
		}
       
	</style>   
    </head>
    <body class='container-fluid' style='background-color: black'>
          <?php 
                include("config.php");
                
                $date_today = date("Y-m-d");                  
                $current_week = (INT)date('W');
                $current_month = date('n');
                $current_year = date('Y');
                $Total = '';
                $ShaneTotalToday = 0;
                $EllaTotalToday = 0;
                $TaylorTotalToday = 0;
				$KeyAccountsTotalToday = 0;
                $TotalToday = 0;
                
                $queryInd = "SELECT sum(IF(group_id = 1, points_value, 0)) as Shane, sum(IF(group_id = 2, points_value, 0)) as Ella, sum(IF(group_id = 3, points_value, 0)) as Taylor, sum(IF(group_id = 4, points_value, 0)) as KeyAccount FROM jobs WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1";
                $output = mysqli_query($link,$queryInd);     
                while ($Total = mysqli_fetch_array($output, MYSQLI_ASSOC)) {
                    $ShaneTotalToday +=  $Total['Shane'];
                    $EllaTotalToday +=  $Total['Ella'] ;
                    $TaylorTotalToday +=  $Total['Taylor'] ;
					$KeyAccountsTotalToday += $Total['KeyAccount'];
                    $TotalToday = $ShaneTotalToday + $EllaTotalToday + $TaylorTotalToday + $KeyAccountsTotalToday;
            }
          ?>
        <br />
        <div class='row'>
            <div class='col-xs-5ths col-md-6'>
                 <div class="panel panel-black">
                    <div class="panel-heading panel-heading-custom">
                        <div class="row">
                            <p style='font-size:50px;text-align: center'><?php echo "&pound;".number_format($TotalToday,2) ?></p>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <h4 style='text-align: center'>TOTAL(Today)</h4>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class='col-xs-5ths col-md-6'>
                 <div class="panel panel-red">
                    <div class="panel-heading panel-heading-custom">
                        <div class="row">
                            <p style='font-size:50px;text-align: center'><?php echo "&pound;".number_format($ShaneTotalToday,2) ?></p>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <h4 style='text-align: center'>REGULATORS</h4>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class='col-xs-5ths col-md-6'>
                 <div class="panel panel-primary">
                    <div class="panel-heading panel-heading-custom">
                        <div class="row">
                            <p style='font-size:50px;text-align: center'><?php echo "&pound;".number_format($EllaTotalToday,2) ?></p>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <h4 style='text-align: center'>FIELD OF DREAMS</h4>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>           
            <div class='col-xs-5ths col-md-6'>
                 <div class="panel panel-yellow">
                    <div class="panel-heading panel-heading-custom">
                        <div class="row">
                            <p style='font-size:50px;text-align: center'><?php echo "&pound;".number_format($TaylorTotalToday,2)?></p>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <h4 style='text-align: center'>ACADEMY</h4>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>   
<!--
			<div class='col-xs-5ths col-md-6'>
                 <div class="panel panel-green">
                    <div class="panel-heading panel-heading-custom">
                        <div class="row">
                            <p style='font-size:50px;text-align: center'><?php echo "&pound;".number_format($KeyAccountsTotalToday,2)?></p>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <h4 style='text-align: center'>MEGA-DONS</h4>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>         			
-->
            </div>
                  
    <?php 
    $query = "SELECT booked_date, sum(IF(group_id = 1, points_value, 0)) as WetWasteMafia, sum(IF(group_id = 2, points_value, 0)) as IncredibleBulks, sum(IF(group_id = 3, points_value, 0)) as StreetSweepers, sum(IF(group_id = 4, points_value, 0)) as MegaDons  FROM jobs WHERE (booked_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW() && Cancelled != 1 && Removed != 1) GROUP BY booked_date ORDER BY booked_date ASC";
    $result = mysqli_query($link,$query);  
	$rows = array();
	while($j = mysqli_fetch_assoc($result)) {
		$rows[] = $j;
	}
    
    $query2 = "SELECT booked_date, sum(IF(group_id = 1, 1, 0)) as WetWasteMafia, sum(IF(group_id = 2, 1, 0)) as IncredibleBulks, sum(IF(group_id = 3, 1, 0)) as StreetSweepers, sum(IF(group_id = 4, 1, 0)) as MegaDons FROM apps WHERE (booked_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW() && Cancelled != 1 && Removed != 1) GROUP BY booked_date ORDER BY booked_date ASC";
    $result2 = mysqli_query($link,$query2);   
	$Apps = array();
		while($a = mysqli_fetch_assoc($result2)) {
			$Apps[] = $a;
		}	
    ?>
            
            
            <div class='col-lg-6' id="morris-line-chart-Jobs" style='height: 350px;'>
                <h4 style='margin-top:-330px;text-align:center;color: #ecf0f1'>Jobs Won Graph</h4>
            </div>
            <div class='col-lg-6' id="morris-line-chart-Apps" style='height: 350px;'>
                <h4 style='margin-top:-330px;text-align:center;color:#ecf0f1'> Appointments Graph</h4>
            </div>
            <br />   
            <br />
        
        <?php
            //Month's Leader Board
                $GoldMonthName = '';
                $SilverMonthName = '';
                $BronzeMonthName = '';
                $GoldMonthValue = 0;
                $SilverMonthValue = 0;
                $BronzeMonthValue = 0;                
                $TotalMonth = 0;
                
                $queryMonthLeaders = "SELECT agent_name, SUM(points_value) as points_value FROM jobs WHERE (YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1) GROUP BY agent_name ORDER BY SUM(points_value) DESC Limit 3";
                $outputMonthLeaders = mysqli_query($link,$queryMonthLeaders);   
                $iMonth = 1;
                 while ( $TotalMonthLeaders = mysqli_fetch_array($outputMonthLeaders, MYSQLI_ASSOC)) {
                     if($iMonth == 1){
                        $GoldMonthName .= $TotalMonthLeaders['agent_name'];
                        $GoldMonthValue += $TotalMonthLeaders['points_value'];
                     }
                     else if($iMonth == 2){
                        $SilverMonthName .= $TotalMonthLeaders['agent_name'];
                        $SilverMonthValue += $TotalMonthLeaders['points_value'];
                     }
                     elseif($iMonth == 3){
                        $BronzeMonthName = $TotalMonthLeaders['agent_name'];
                        $BronzeMonthValue += $TotalMonthLeaders['points_value'];
                     }                        
                    $iMonth++;
            }       
                    $queryTotalMonth = "SELECT sum(points_value) as points_value from jobs WHERE YEAR(booked_date) = $current_year && MONTH(booked_date) = $current_month && Cancelled != 1 && Removed != 1";
                        $outputTotalMonth = mysqli_query($link,$queryTotalMonth);     
                        while ($TotalTotalMonth = mysqli_fetch_array($outputTotalMonth, MYSQLI_ASSOC)) {
                            $TotalMonth +=  $TotalTotalMonth['points_value'];
                    }
                        $PercentageGoldMonth = round(($GoldMonthValue * 100)/$TotalMonth);
                        $PercentageSilverMonth = round(($SilverMonthValue * 100)/$TotalMonth);
                        $PercentageBronzeMonth = round(($BronzeMonthValue * 100)/$TotalMonth);
        ?> 
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-orange">
                        <div class="panel-heading panel-heading-custom">
                            <h4>Month's Leader Board</h4>
                        </div>
                        <div class="panel-body panel-bodyOne">
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $GoldMonthName == NULL || !file_exists("img/sales/".str_replace(' ', '', $GoldMonthName).".jpg") ? "NoImage.png" : str_replace(' ', '', $GoldMonthName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $GoldMonthName == NULL ? "None" : $GoldMonthName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($GoldMonthValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $PercentageGoldMonth; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageGoldMonth; ?>%">
                                          <?php echo $PercentageGoldMonth; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/GoldMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $SilverMonthName == NULL || !file_exists("img/sales/".str_replace(' ', '', $SilverMonthName).".jpg") ? "NoImage.png" : str_replace(' ', '', $SilverMonthName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $SilverMonthName == NULL ? "None" : $SilverMonthName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($SilverMonthValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $PercentageSilverMonth; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageSilverMonth; ?>%">
                                          <?php echo $PercentageSilverMonth; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/SilverMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $BronzeMonthName == NULL || !file_exists("img/sales/".str_replace(' ', '', $BronzeMonthName).".jpg") ? "NoImage.png" : str_replace(' ', '', $BronzeMonthName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $BronzeMonthName == NULL ? "None" : $BronzeMonthName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($BronzeMonthValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $PercentageBronzeMonth; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageBronzeMonth; ?>%">
                                          <?php echo $PercentageBronzeMonth; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/BronzeMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                 
                 <?php
                    //Week's Leader Board
                        $GoldWeekName = '';
                        $SilverWeekName = '';
                        $BronzeWeekName = '';
                        $GoldWeekValue = 0;
                        $SilverWeekValue = 0;
                        $BronzeWeekValue = 0;
                        $TotalWeek = 0;
                        
                        $queryWeekLeaders = "SELECT agent_name, SUM(points_value) as points_value FROM jobs WHERE (YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week  && Cancelled != 1 && Removed != 1) GROUP BY agent_name ORDER BY SUM(points_value) DESC Limit 3";
                        $outputWeekLeaders = mysqli_query($link,$queryWeekLeaders);   
                        $iWeek = 1;
                         while ( $TotalWeekLeaders = mysqli_fetch_array($outputWeekLeaders, MYSQLI_ASSOC)) {
                             if($iWeek == 1){
                                $GoldWeekName .= $TotalWeekLeaders['agent_name'];
                                $GoldWeekValue += $TotalWeekLeaders['points_value'];
                             }
                             else if($iWeek == 2){
                                $SilverWeekName .= $TotalWeekLeaders['agent_name'];
                                $SilverWeekValue += $TotalWeekLeaders['points_value'];
                             }
                             elseif($iWeek == 3){
                                $BronzeWeekName = $TotalWeekLeaders['agent_name'];
                                $BronzeWeekValue += $TotalWeekLeaders['points_value'];
                             }                        
                            $iWeek++;
                    }     
                        $queryTotalWeek = "SELECT sum(points_value) as points_value from jobs WHERE YEAR(booked_date) = $current_year && WEEKOFYEAR(booked_date) = $current_week && Cancelled != 1 && Removed != 1";
                        $outputTotalWeek = mysqli_query($link,$queryTotalWeek);     
                        while ($TotalTotalWeek = mysqli_fetch_array($outputTotalWeek, MYSQLI_ASSOC)) {
                            $TotalWeek +=  $TotalTotalWeek['points_value'];
                    }
                        $PercentageGoldWeek = round(($GoldWeekValue * 100)/$TotalWeek);
                        $PercentageSilverWeek = round(($SilverWeekValue * 100)/$TotalWeek);
                        $PercentageBronzeWeek = round(($BronzeWeekValue * 100)/$TotalWeek);
                ?> 
                
                <div class="col-lg-4">
                    <div class="panel panel-orange">
                        <div class="panel-heading panel-heading-custom">
                             <h4>Week's Leader Board</h4>
                        </div>
                        <div class="panel-body panel-bodyTwo">
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $GoldWeekName == NULL || !file_exists("img/sales/".str_replace(' ', '', $GoldWeekName).".jpg") ? "NoImage.png" : str_replace(' ', '', $GoldWeekName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $GoldWeekName == NULL ? "None" : $GoldWeekName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($GoldWeekValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $PercentageGoldWeek; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $PercentageGoldWeek; ?>%">
                                          <?php echo $PercentageGoldWeek; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/GoldMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $SilverWeekName == NULL || !file_exists("img/sales/".str_replace(' ', '', $SilverWeekName).".jpg") ? "NoImage.png" : str_replace(' ', '', $SilverWeekName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $SilverWeekName == NULL ? "None" : $SilverWeekName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($SilverWeekValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $PercentageSilverWeek; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageSilverWeek; ?>%">
                                          <?php echo $PercentageSilverWeek; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/SilverMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $BronzeWeekName == NULL || !file_exists("img/sales/".str_replace(' ', '', $BronzeWeekName).".jpg") ? "NoImage.png" : str_replace(' ', '', $BronzeWeekName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $BronzeWeekName == NULL ? "None" : $BronzeWeekName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($BronzeWeekValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $PercentageBronzeWeek; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageBronzeWeek; ?>%">
                                          <?php echo $PercentageBronzeWeek; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/BronzeMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                      
                    </div>
                
                
                 <?php
                    //Todays's Leader Board
                        $GoldTodayName = '';
                        $SilverTodayName = '';
                        $BronzeTodayName = '';
                        $GoldTodayValue = 0;
                        $SilverTodayValue = 0;
                        $BronzeTodayValue = 0;
                        $TotalToday = 0;

                        $queryTodayLeaders = "SELECT agent_name, SUM(points_value) as points_value FROM jobs WHERE (booked_date = '$date_today' && Cancelled != 1 && Removed != 1) GROUP BY agent_name ORDER BY SUM(points_value) DESC Limit 3";
                        $outputTodayLeaders = mysqli_query($link,$queryTodayLeaders);   
                        $iToday = 1;
                         while ( $TotalTodayLeaders = mysqli_fetch_array($outputTodayLeaders, MYSQLI_ASSOC)) {
                             if($iToday == 1){
                                $GoldTodayName .= $TotalTodayLeaders['agent_name'];
                                $GoldTodayValue += $TotalTodayLeaders['points_value'];
                             }
                             else if($iToday == 2){
                                $SilverTodayName .= $TotalTodayLeaders['agent_name'];
                                $SilverTodayValue += $TotalTodayLeaders['points_value'];
                             }
                             elseif($iToday == 3){
                                $BronzeTodayName = $TotalTodayLeaders['agent_name'];
                                $BronzeTodayValue += $TotalTodayLeaders['points_value'];
                             }                        
                            $iToday++;
                    }
                        $queryTotalToday = "SELECT sum(points_value) as points_value from jobs WHERE booked_date = '$date_today' && Cancelled != 1 && Removed != 1";
                        $outputTotalToday = mysqli_query($link,$queryTotalToday);     
                        while ($TotalTotalToday = mysqli_fetch_array($outputTotalToday, MYSQLI_ASSOC)) {
                            $TotalToday +=  $TotalTotalToday['points_value'];
                    }
                        $PercentageGoldToday = round(($GoldTodayValue * 100)/$TotalToday);
                        $PercentageSilverToday = round(($SilverTodayValue * 100)/$TotalToday);
                        $PercentageBronzeToday = round(($BronzeTodayValue * 100)/$TotalToday);
                ?> 
                
                
                <div class="col-lg-4">
                    <div class="panel panel-orange">
                        <div class="panel-heading panel-heading-custom">
                             <h4>Today's Leader Board</h4>
                        </div>
                        <div class="panel-body panel-bodyThree">
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $GoldTodayName == NULL || !file_exists("img/sales/".str_replace(' ', '', $GoldTodayName).".jpg") ? "NoImage.png" : str_replace(' ', '', $GoldTodayName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $GoldTodayName == NULL ? "None" : $GoldTodayName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($GoldTodayValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $PercentageGoldToday; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageGoldToday; ?>%">
                                          <?php echo $PercentageGoldToday; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/GoldMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $SilverTodayName == NULL || !file_exists("img/sales/".str_replace(' ', '', $SilverTodayName).".jpg") ? "NoImage.png" : str_replace(' ', '', $SilverTodayName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px;height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $SilverTodayName == NULL ? "None" : $SilverTodayName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($SilverTodayValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $PercentageSilverToday; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageSilverToday; ?>%">
                                          <?php echo $PercentageSilverToday; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/SilverMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-3'>
                                    <img src="img/sales/<?php echo $BronzeTodayName == NULL || !file_exists("img/sales/".str_replace(' ', '', $BronzeTodayName).".jpg") ? "NoImage.png" : str_replace(' ', '', $BronzeTodayName).".jpg?1222259157.415"; ?>" class='img-responsive img-circle' style="width: 80px; height:90px;"/>
                                </div>
                                <div class='col-lg-6'>
                                    <h4 style="color:white"><?php echo $BronzeTodayName == NULL ? "None" : $BronzeTodayName ?></h4>
                                    <h4 style="color:white"><?php echo "&pound;".number_format($BronzeTodayValue,2); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $PercentageBronzeToday; ?>"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $PercentageBronzeToday; ?>%">
                                          <?php echo $PercentageBronzeToday; ?>% of Total
                                        </div>
                                    </div>
                                </div>
                                <div class='col-lg-3'>
                                    <img src="img/BronzeMedal.png" class="img-responsive img-circle" style="height:80px;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
					
		<script type="text/javascript"> 
			Morris.Area({
				// ID of the element in which to draw the chart.
				element: 'morris-line-chart-Jobs',
			 
				// Chart data records -- each entry in this array corresponds to a point
				// on the chart.
				data: <?php echo json_encode($rows);?>,
			 
				// The name of the data record attribute that contains x-values.
				xkey: ['booked_date'],
			 
				// A list of names of data record attributes that contain y-values.
				ykeys: ['WetWasteMafia','IncredibleBulks','StreetSweepers','Megadons'],
			 
				// Labels for the ykeys -- will be displayed when you hover over the
				// chart.
				labels: ['Regulators','Field of Dreams','Academy',''],
			 
				lineColors: ['#E24E42', '#337ab7','#E9B000','#E98111'],
				xLabels: 'Date',
			 
				// Disables line smoothing
				smooth: true,
				resize: true,    
				behaveLikeLine: true,
				  pointFillColors:['#ffffff'],
				  pointStrokeColors: ['black'],
			});
			Morris.Line({
			element: 'morris-line-chart-Apps',
			data: <?php echo json_encode($Apps);?>,
						xkey: ['booked_date'],
						ykeys: ['WetWasteMafia','IncredibleBulks','StreetSweepers','Megadons'],
						labels: ['Regulators','Field of Dreams','Academy',''],
						lineColors: ['#E24E42', '#337ab7', '#E9B000', '#E98111'],
						xLabels: 'Date',
						smooth: true,
						resize: true,    
						behaveLikeLine: true,
						pointFillColors:['#ffffff'],
						pointStrokeColors: ['black'],
			});
	</script>
        
        

    </body>
</html>
