<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<title>Hydro-Board Wallboard</title>
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<meta http-equiv="refresh" content="30" >
</head>

<?php include("config.php"); ?>

<body>

<div id="container" >

	<div id="headings">
		<div id="h_label"></div>
		<div id="h_day"><h3><?php echo date("l",mktime());?></h3></div>
		<div id="top_logo"></div>
		<div id="h_week"><h3>Week</h3></div>
	</div>

<?php

// Green Main Target
$target_jt_day = 125/5;
$target_jw_week = 125;
$target_pt_day = 813/5;
$target_pw_week = 813;
$target_at_day = 15/5;
$target_aw_week = 15;

// Gold Threshold
$target_jt2_day = 150/5;
$target_jw2_week = 150;
$target_pt2_day = 975/5;
$target_pw2_week = 975;
$target_at2_day = 20/5;
$target_aw2_week = 20;

// START - DATE SETUP (Change these to query specific dates/weeks)

$date_today = date("Y-m-d");
$day_number = date('w');
$week_begin = date("Y-m-d",time() - ($day_number)*60*60*24);

// END - DATE SETUP

$current_hour = date(G);
$current_min = date(i);

$target_jt_now = ($current_hour - 9) * $target_jt_day/8 + ($current_min * $target_jt_day/(8*60)); 
$target_jw_now = ($day_number - 1) * $target_jt_day + $target_jt_now; 

$target_pt_now = ($current_hour - 9) * $target_pt_day/8 + ($current_min * $target_pt_day/(8*60));
$target_pw_now = ($day_number - 1) * $target_pt_day + $target_pt_now; 

$target_at_now = ($current_hour - 9) * $target_at_day/8 + ($current_min * $target_at_day/(8*60)); 
$target_aw_now = ($day_number - 1) * $target_at_day + $target_at_now; 

$query = "SELECT SUM(job_value),SUM(points_value) FROM jobs WHERE booked_date='$date_today' && cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jt = $row['SUM(job_value)'];
$pt = $row['SUM(points_value)'];
/*$jt = 150/5;
$pt = 975/5;*/


$query = "SELECT SUM(job_value),SUM(points_value) FROM jobs WHERE booked_date >= '$week_begin' && cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$row = mysql_fetch_array($result);
$jw= $row['SUM(job_value)'];
$pw= $row['SUM(points_value)'];
/*$jw= 150;
$pw= 975;*/

$query = "SELECT * FROM apps WHERE booked_date ='$date_today' && cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$at = mysql_num_rows($result);
/*$at = 20/5;*/

$query = "SELECT * FROM apps WHERE booked_date >= '$week_begin' && cancelled = '0'";
$result = mysql_query($query) or die(mysql_error());
$aw = mysql_num_rows($result);
/*$aw = 20;*/

if ($jt > $target_jt_day) $target_jt_now = $target_jt_day;
if ($jw > $target_jw_week) $target_jw_now = $target_jw_week;
if ($pt > $target_pt_day) $target_pt_now = $target_pt_day;
if ($pw > $target_pw_week) $target_pw_now = $target_pw_week;
if ($at > $target_at_day) $target_at_now = $target_at_day;
if ($aw > $target_aw_week) $target_aw_now = $target_aw_week;

// Board Display Calcualtion (floor removes all digits beyond the decimal)
$jt1 = floor($jt/100);
$jt2 = floor(($jt - $jt1*100 )/10);
$jt3 = floor(($jt - $jt1*100 - $jt2*10));
$jw1 = floor($jw/100);
$jw2 = floor(($jw - $jw1*100 )/10);
$jw3 = floor(($jw - $jw1*100 - $jw2*10));
$pt0 = floor($pt/1000);
$pt1 = floor(($pt - $pt0*1000 )/100);
$pt2 = floor(($pt - $pt0*1000 - $pt1*100 )/10);
$pt3 = floor(($pt - $pt0*1000 - $pt1*100 - $pt2*10));
$pw0 = floor($pw/1000);
$pw1 = floor(($pw - $pw0*1000 )/100);
$pw2 = floor(($pw - $pw0*1000 - $pw1*100 )/10);
$pw3 = floor(($pw - $pw0*1000 - $pw1*100 - $pw2*10));
$at1 = floor($at/100);
$at2 = floor(($at - $at1*100 )/10);
$at3 = floor(($at - $at1*100 - $at2*10));
$aw1 = floor($aw/100);
$aw2 = floor(($aw - $aw1*100 )/10);
$aw3 = floor(($aw - $aw1*100 - $aw2*10));

?>

	<br><br>
	<div id="jobs">
			<div id="jobs_label"><h2>Jobs</h2></div>
			<div style="float: right">
				<div id="jobs_today">
					<div id="jt"></div>
					<div id="jt"><img src="images/<?php if ($jt >= $target_jt2_day) echo 'gold/'; else if ($jt >= $target_jt_day) echo 'green/'; else if ($jt > $target_jt_now) echo 'orange/'; else echo 'red/';?><?php if ($jt1==0) echo("blank"); else echo($jt1)?>.gif" ></div>
					<div id="jt"><img src="images/<?php if ($jt >= $target_jt2_day) echo 'gold/'; else if ($jt >= $target_jt_day) echo 'green/'; else if ($jt > $target_jt_now) echo 'orange/'; else echo 'red/';?><?php if ($jt1==0 && $jt2==0) echo("blank"); else echo($jt2)?>.gif" ></div>
					<div id="jt"><img src="images/<?php if ($jt >= $target_jt2_day) echo 'gold/'; else if ($jt >= $target_jt_day) echo 'green/'; else if ($jt > $target_jt_now) echo 'orange/'; else echo 'red/';?><?php echo($jt3)?>.gif" ></div>
				</div>
				<div id="jobs_week">
					<div id="jw"></div>
					<div id="jw"><img src="images/<?php if ($jw >= $target_jw2_week) echo 'gold/'; else if ($jw >= $target_jw_week) echo 'green/'; else if ($jw > $target_jw_now) echo 'orange/'; else echo 'red/';?><?php if ($jw1==0) echo("blank"); else echo($jw1)?>.gif" ></div>
					<div id="jw"><img src="images/<?php if ($jw >= $target_jw2_week) echo 'gold/'; else if ($jw >= $target_jw_week) echo 'green/'; else if ($jw > $target_jw_now) echo 'orange/'; else echo 'red/';?><?php if ($jw1==0 && $jw2==0) echo("blank"); else echo($jw2)?>.gif" ></div>
					<div id="jw"><img src="images/<?php if ($jw >= $target_jw2_week) echo 'gold/'; else if ($jw >= $target_jw_week) echo 'green/'; else if ($jw > $target_jw_now) echo 'orange/'; else echo 'red/';?><?php echo($jw3)?>.gif" ></div>
				</div>
			</div>
	</div>
	<div id="points">
			<div id="points_label"><h2>Points</h2></div>
			<div style="float: right">
				<div id="points_today">
					<div id="pt"><img src="images/<?php if ($pt >= $target_pt2_day) echo 'gold/'; else if ($pt >= $target_pt_day) echo 'green/'; else if ($pt > $target_pt_now) echo 'orange/'; else echo 'red/';?><?php if ($pt0==0) echo("blank"); else echo($pt0)?>.gif"></div>
					<div id="pt"><img src="images/<?php if ($pt >= $target_pt2_day) echo 'gold/'; else if ($pt >= $target_pt_day) echo 'green/'; else if ($pt > $target_pt_now) echo 'orange/'; else echo 'red/';?><?php if ($pt0==0 && $pt1==0) echo("blank"); else echo($pt1)?>.gif"></div>
					<div id="pt"><img src="images/<?php if ($pt >= $target_pt2_day) echo 'gold/'; else if ($pt >= $target_pt_day) echo 'green/'; else if ($pt > $target_pt_now) echo 'orange/'; else echo 'red/';?><?php if ($pt0==0 && $pt1==0 && $pt2==0) echo("blank"); else echo($pt2)?>.gif"></div>
					<div id="pt"><img src="images/<?php if ($pt >= $target_pt2_day) echo 'gold/'; else if ($pt >= $target_pt_day) echo 'green/'; else if ($pt > $target_pt_now) echo 'orange/'; else echo 'red/';?><?php echo($pt3)?>.gif"></div>
				</div>
				<div id="points_week">
					<div id="pw"><img src="images/<?php if ($pw >= $target_pw2_week) echo 'gold/'; else if ($pw >= $target_pw_week) echo 'green/'; else if ($pw > $target_pw_now) echo 'orange/'; else echo 'red/';?><?php if ($pw0==0) echo("blank"); else echo($pw0)?>.gif"></div>
					<div id="pw"><img src="images/<?php if ($pw >= $target_pw2_week) echo 'gold/'; else if ($pw >= $target_pw_week) echo 'green/'; else if ($pw > $target_pw_now) echo 'orange/'; else echo 'red/';?><?php if ($pw0==0 && $pw1==0) echo("blank"); else echo($pw1)?>.gif"></div>
					<div id="pw"><img src="images/<?php if ($pw >= $target_pw2_week) echo 'gold/'; else if ($pw >= $target_pw_week) echo 'green/'; else if ($pw > $target_pw_now) echo 'orange/'; else echo 'red/';?><?php if ($pw0==0 && $pw1==0 && $pw2==0) echo("blank"); else echo($pw2)?>.gif"></div>
					<div id="pw"><img src="images/<?php if ($pw >= $target_pw2_week) echo 'gold/'; else if ($pw >= $target_pw_week) echo 'green/'; else if ($pw > $target_pw_now) echo 'orange/'; else echo 'red/';?><?php echo($pw3)?>.gif"></div>
				</div>
			</div>
	</div>
	<div id="appointments" >
			<div id="appointments_label"><h2>Apps</h2></div>
			<div style="float: right">
				<div id="appointments_today" >
					<div id="at"></div>
					<div id="at"><img src="images/<?php if ($at >= $target_at2_day) echo 'gold/'; else if ($at >= $target_at_day) echo 'green/'; else if ($at > $target_at_now) echo 'orange/'; else echo 'red/';?><?php if ($at1==0) echo("blank"); else echo($at1)?>.gif" ></div>
					<div id="at"><img src="images/<?php if ($at >= $target_at2_day) echo 'gold/'; else if ($at >= $target_at_day) echo 'green/'; else if ($at > $target_at_now) echo 'orange/'; else echo 'red/';?><?php if ($at1==0 && $at2==0) echo("blank"); else echo($at2)?>.gif" ></div>
					<div id="at"><img src="images/<?php if ($at >= $target_at2_day) echo 'gold/'; else if ($at >= $target_at_day) echo 'green/'; else if ($at > $target_at_now) echo 'orange/'; else echo 'red/';?><?php echo($at3)?>.gif"></div>
				</div>
				<div id="appointments_week">
					<div id="aw"></div>
					<div id="aw"><img src="images/<?php if ($aw >= $target_aw2_week) echo 'gold/'; else if ($aw >= $target_aw_week) echo 'green/'; else if ($aw > $target_aw_now) echo 'orange/'; else echo 'red/';?><?php if ($aw1==0) echo("blank"); else echo($aw1)?>.gif" ></div>
					<div id="aw"><img src="images/<?php if ($aw >= $target_aw2_week) echo 'gold/'; else if ($aw >= $target_aw_week) echo 'green/'; else if ($aw > $target_aw_now) echo 'orange/'; else echo 'red/';?><?php if ($aw1==0 && $aw2==0) echo("blank"); else echo($aw2)?>.gif" ></div>
					<div id="aw"><img src="images/<?php if ($aw >= $target_aw2_week) echo 'gold/'; else if ($aw >= $target_aw_week) echo 'green/'; else if ($aw > $target_aw_now) echo 'orange/'; else echo 'red/';?><?php echo($aw3)?>.gif"></div>
				</div>
			</div>
	</div>

</div>

</body>
</html>